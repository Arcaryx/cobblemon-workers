package com.arcaryx.cobblemonworkers.fabric

import com.arcaryx.cobblemonworkers.CobblemonWorkers
import com.arcaryx.cobblemonworkers.IPluginLoader
import com.arcaryx.cobblemonworkers.PluginRegistry
import com.arcaryx.cobblemonworkers.api.CWPlugin
import com.arcaryx.cobblemonworkers.api.ICWPlugin
import com.arcaryx.cobblemonworkers.api.ICWPluginConfig
import com.google.common.base.Strings
import net.fabricmc.loader.api.FabricLoader

class PluginLoaderFabric : IPluginLoader {

    init {
        load()
    }

    private fun isModLoaded(modid: String?) = FabricLoader.getInstance().isModLoaded(modid)

    // https://github.com/Snownee/Jade/blob/4b4caa89a98d9ba067285212d5d5f76d67124f81/src/main/java/snownee/jade/util/CommonProxy.java#L349
    override fun load() {
        val classes: MutableSet<Class<*>> = mutableSetOf()
        for (entrypoint in FabricLoader.getInstance().getEntrypointContainers("cw_plugin", ICWPlugin::class.java)) {
            val metadata = entrypoint.provider.metadata
            CobblemonWorkers.LOGGER.info("Loading plugin at ${metadata.name}")
            var className: String? = null
            try {
                val plugin = entrypoint.entrypoint
                val annotation = plugin.javaClass.getDeclaredAnnotation(CWPlugin::class.java)
                if (annotation != null && !Strings.isNullOrEmpty(annotation.value) && !isModLoaded(annotation.value)) {
                    continue
                }
                className = plugin.javaClass.name
                if (!classes.add(plugin.javaClass)) {
                    throw IllegalStateException("Duplicate plugin class $className")
                }
                PluginRegistry.register(plugin as ICWPlugin<ICWPluginConfig>)
            } catch (ex: Throwable) {
                CobblemonWorkers.LOGGER.error("Error loading plugin at $className", ex)
            }
        }
    }
}