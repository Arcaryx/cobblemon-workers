package com.arcaryx.cobblemonworkers.fabric

import com.arcaryx.cobblemonworkers.CWPluginMapping
import com.arcaryx.cobblemonworkers.CWPokemonMapping
import com.arcaryx.cobblemonworkers.CobblemonWorkers
import com.arcaryx.cobblemonworkers.client.gui.ItemFilterScreen
import com.arcaryx.cobblemonworkers.client.gui.ItemFilterScreenHandler
import net.fabricmc.api.ModInitializer
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerLifecycleEvents
import net.fabricmc.fabric.api.event.registry.DynamicRegistries
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerType
import net.minecraft.registry.Registries
import net.minecraft.registry.Registry
import net.minecraft.registry.RegistryKey
import net.minecraft.screen.ScreenHandlerType
import net.minecraft.server.MinecraftServer
import net.minecraft.util.Identifier

class CobblemonWorkersFabric : ModInitializer {

    override fun onInitialize() {
        CobblemonWorkers.init()
        CobblemonWorkers.furnaceAccessor = FurnaceAccessorFabric()

        ServerLifecycleEvents.SERVER_STARTING.register(ServerLifecycleEvents.ServerStarting { server: MinecraftServer ->
            if (server.isDedicated) {
                CobblemonWorkers.pluginLoader = PluginLoaderFabric()
            }
        })

        CWPluginMapping.KEY = RegistryKey.ofRegistry(Identifier(CWPluginMapping.NAME))
        DynamicRegistries.register(CWPluginMapping.KEY, CWPluginMapping.CODEC)

        CWPokemonMapping.KEY = RegistryKey.ofRegistry(Identifier(CWPokemonMapping.NAME))
        DynamicRegistries.register(CWPokemonMapping.KEY, CWPokemonMapping.CODEC)

        val itemFilterHandlerType: ScreenHandlerType<ItemFilterScreenHandler> = ExtendedScreenHandlerType {
            syncId, inventory, _ -> ItemFilterScreenHandler(syncId, inventory) // TODO: Something needs to be done with buf?
        }
        CobblemonWorkers.ITEM_FILTER_SCREEN_HANDLER = Registry.register(Registries.SCREEN_HANDLER, Identifier(CobblemonWorkers.MOD_ID, ItemFilterScreen.NAME), itemFilterHandlerType)
        CobblemonWorkers.ITEM_FILTER_SCREEN_HANDLER_FACTORY = ItemFilterScreenFactoryFabric()
    }
}