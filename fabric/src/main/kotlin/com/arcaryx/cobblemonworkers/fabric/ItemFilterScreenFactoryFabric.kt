package com.arcaryx.cobblemonworkers.fabric

import com.arcaryx.cobblemonworkers.client.gui.ItemFilterScreen
import com.arcaryx.cobblemonworkers.client.gui.ItemFilterScreenHandler
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.network.PacketByteBuf
import net.minecraft.screen.ScreenHandler
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Text

class ItemFilterScreenFactoryFabric : ExtendedScreenHandlerFactory {
    override fun createMenu(syncId: Int, playerInventory: PlayerInventory, player: PlayerEntity): ScreenHandler {
        return ItemFilterScreenHandler(syncId, playerInventory)
    }

    override fun getDisplayName(): Text {
        return ItemFilterScreen.TITLE
    }

    override fun writeScreenOpeningData(player: ServerPlayerEntity, buf: PacketByteBuf) {
        // TODO: ???
    }
}