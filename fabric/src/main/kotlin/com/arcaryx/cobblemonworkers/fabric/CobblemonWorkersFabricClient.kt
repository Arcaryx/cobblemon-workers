package com.arcaryx.cobblemonworkers.fabric

import com.arcaryx.cobblemonworkers.CobblemonWorkers
import com.arcaryx.cobblemonworkers.client.gui.ItemFilterScreen
import net.fabricmc.api.ClientModInitializer
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientLifecycleEvents
import net.minecraft.client.gui.screen.ingame.HandledScreens

class CobblemonWorkersFabricClient : ClientModInitializer {
    override fun onInitializeClient() {
        CobblemonWorkers.initClient()

        ClientLifecycleEvents.CLIENT_STARTED.register(ClientLifecycleEvents.ClientStarted {
            CobblemonWorkers.pluginLoader = PluginLoaderFabric()
        })

        HandledScreens.register(CobblemonWorkers.ITEM_FILTER_SCREEN_HANDLER, ::ItemFilterScreen)
    }
}