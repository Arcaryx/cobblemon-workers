package com.arcaryx.cobblemonworkers.fabric

import com.arcaryx.cobblemonworkers.IFurnaceAccessor
import com.arcaryx.cobblemonworkers.fabric.mixin.InvokerAbstractFurnaceBlockEntity
import net.minecraft.block.entity.AbstractFurnaceBlockEntity
import net.minecraft.item.ItemStack
import net.minecraft.recipe.Recipe
import net.minecraft.registry.DynamicRegistryManager
import net.minecraft.util.collection.DefaultedList

class FurnaceAccessorFabric : IFurnaceAccessor {
    override fun canAcceptRecipeOutput(
        blockEntity: AbstractFurnaceBlockEntity,
        registryManager: DynamicRegistryManager?,
        recipe: Recipe<*>?,
        slots: DefaultedList<ItemStack?>?,
        count: Int
    ): Boolean {
        return InvokerAbstractFurnaceBlockEntity.canAcceptRecipeOutput(registryManager, recipe, slots, count)
    }
}