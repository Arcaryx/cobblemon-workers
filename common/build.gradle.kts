plugins {
    id("cw.base-conventions")
    id("org.jetbrains.gradle.plugin.idea-ext")
}

architectury {
    common("forge", "fabric")
}

repositories {
    maven(url = "${rootProject.projectDir}/deps")
    maven(url = "https://api.modrinth.com/maven")
    mavenLocal()
}

dependencies {
    implementation(libs.bundles.kotlin)
    modImplementation(libs.fabric.loader)

    // Integrations
    modCompileOnly(libs.bundles.fabric.integrations.compileOnly) {
        isTransitive = false
    }

}