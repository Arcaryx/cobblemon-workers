package com.arcaryx.cobblemonworkers.mixin;

import com.arcaryx.cobblemonworkers.WorkerAssignmentGoal;
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.passive.TameableShoulderEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(PokemonEntity.class)
abstract class MixinPokemonEntity extends TameableShoulderEntity {
    protected MixinPokemonEntity(EntityType<? extends TameableShoulderEntity> entityType, World world) {
        super(entityType, world);
    }

    @Inject(method = "initGoals", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/ai/goal/GoalSelector;add(ILnet/minecraft/entity/ai/goal/Goal;)V"))
    private void initGoalsFirst(CallbackInfo ci) {
        this.goalSelector.add(0, new WorkerAssignmentGoal((PokemonEntity) ((Object) this)));
    }
}
