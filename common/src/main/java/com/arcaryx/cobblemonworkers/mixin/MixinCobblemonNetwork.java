package com.arcaryx.cobblemonworkers.mixin;

import com.arcaryx.cobblemonworkers.CobblemonWorkers;
import com.cobblemon.mod.common.CobblemonNetwork;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(value = CobblemonNetwork.class, remap = false)
abstract class MixinCobblemonNetwork {

    @Inject(method = "registerServerBound", at = @At("RETURN"))
    private void registerServerBound(CallbackInfo ci) {
        CobblemonWorkers.INSTANCE.registerServerBound();
    }

    @Inject(method = "registerClientBound", at = @At("RETURN"))
    private void registerClientBound(CallbackInfo ci) {
        CobblemonWorkers.INSTANCE.registerClientBound();
    }
}
