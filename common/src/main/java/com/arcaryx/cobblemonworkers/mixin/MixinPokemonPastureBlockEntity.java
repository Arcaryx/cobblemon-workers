package com.arcaryx.cobblemonworkers.mixin;

import com.arcaryx.cobblemonworkers.CWPokemon;
import com.cobblemon.mod.common.api.storage.pc.PCStore;
import com.cobblemon.mod.common.block.entity.PokemonPastureBlockEntity;
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity;
import com.cobblemon.mod.common.pokemon.Pokemon;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin(PokemonPastureBlockEntity.class)
abstract class MixinPokemonPastureBlockEntity {

    @Inject(method = "tether", at = @At(value = "INVOKE", target = "Lcom/cobblemon/mod/common/block/entity/PokemonPastureBlockEntity;markDirty()V"), locals = LocalCapture.CAPTURE_FAILHARD)
    private void tether(ServerPlayerEntity player, Pokemon pokemon, Direction directionToBehind, CallbackInfoReturnable<Boolean> cir, World world, PokemonEntity entity, double width, BlockPos idealPlace, Box box, int i, BlockPos fixedPosition, PCStore pc, PokemonPastureBlockEntity.Tethering tethering) {
        CWPokemon.Companion.tryCreate(entity, (PokemonPastureBlockEntity) ((Object) this));
    }
}
