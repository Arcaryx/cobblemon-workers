package com.arcaryx.cobblemonworkers.mixin;

import com.arcaryx.cobblemonworkers.ServerStateManager;
import net.minecraft.block.AbstractFurnaceBlock;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.AbstractFurnaceBlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.block.entity.LockableContainerBlockEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(AbstractFurnaceBlockEntity.class)
public abstract class MixinAbstractFurnaceBlockEntity extends LockableContainerBlockEntity {
    @Shadow
    int burnTime;

    @Shadow
    int fuelTime;

    protected MixinAbstractFurnaceBlockEntity(BlockEntityType<?> blockEntityType, BlockPos blockPos, BlockState blockState) {
        super(blockEntityType, blockPos, blockState);
    }

    @Inject(method = "tick", at = @At("HEAD"))
    private static void tick(World world, BlockPos pos, BlockState state, AbstractFurnaceBlockEntity blockEntity, CallbackInfo ci) {
        if (ServerStateManager.INSTANCE.hasValidWorker(pos, state, world)) {
            var wasBurning = ((MixinAbstractFurnaceBlockEntity) ((Object) blockEntity)).burnTime > 0;
            ((MixinAbstractFurnaceBlockEntity) ((Object) blockEntity)).burnTime = 20;
            ((MixinAbstractFurnaceBlockEntity) ((Object) blockEntity)).fuelTime = 20;
            if (!wasBurning) {
                state = state.with(AbstractFurnaceBlock.LIT, true);
                world.setBlockState(pos, state, 3);
                markDirty(world, pos, state);
            }
        }
    }
}
