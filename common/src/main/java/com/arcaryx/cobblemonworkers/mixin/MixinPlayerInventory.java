package com.arcaryx.cobblemonworkers.mixin;

import com.arcaryx.cobblemonworkers.client.ClientStateManager;
import net.minecraft.entity.player.PlayerInventory;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(PlayerInventory.class)
abstract class MixinPlayerInventory {
    @Inject(method = "scrollInHotbar", at = @At("HEAD"), cancellable = true)
    private void scrollInHotbar(double scrollAmount, CallbackInfo ci) {
        if (ClientStateManager.INSTANCE.adjustRadius(scrollAmount)) {
            ci.cancel();
        }
    }
}
