package com.arcaryx.cobblemonworkers.mixin;

import net.minecraft.block.entity.AbstractFurnaceBlockEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.AbstractCookingRecipe;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.util.collection.DefaultedList;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(AbstractFurnaceBlockEntity.class)
public interface AccessorAbstractFurnaceBlockEntity {
    @Accessor("matchGetter")
    @Final
    RecipeManager.MatchGetter<Inventory, ? extends AbstractCookingRecipe> getMatchGetter();

    @Accessor("inventory")
    DefaultedList<ItemStack> getInventory();
}
