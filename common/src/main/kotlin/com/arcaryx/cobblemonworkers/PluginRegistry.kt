package com.arcaryx.cobblemonworkers

import com.arcaryx.cobblemonworkers.api.ICWPlugin
import com.arcaryx.cobblemonworkers.api.ICWPluginConfig
import com.cobblemon.mod.common.pokemon.Pokemon
import net.minecraft.registry.DynamicRegistryManager
import net.minecraft.util.Identifier

object PluginRegistry {
    private val plugins: MutableMap<Identifier, ICWPlugin<ICWPluginConfig>> = mutableMapOf()
    private val pluginMap: MutableMap<Pair<Identifier, Identifier>, MutableList<ICWPlugin<ICWPluginConfig>>> = mutableMapOf()
    private val pokemonMap: MutableMap<Pair<String, String>, MutableList<Pair<Identifier, Int>>> = mutableMapOf()

    fun register(plugin: ICWPlugin<ICWPluginConfig>) {
        plugins[plugin.getIdentifier()] = plugin
    }

    fun build(registryManager: DynamicRegistryManager) {
        val pluginRegistry = registryManager.get(CWPluginMapping.KEY)
        for (es in pluginRegistry.entrySet) {
            val mapping = es.value
            val plugin = plugins[mapping.pluginId]
            if (plugin == null) {
                CobblemonWorkers.LOGGER.error("Tried to map for unknown plugin ${mapping.pluginId}")
                continue
            }
            for (entry in mapping.blockMapping) {
                pluginMap.getOrPut(entry.toPair()) { mutableListOf() }.add(plugin)
            }
        }

        val pokemonRegistry = registryManager.get(CWPokemonMapping.KEY)
        for (es in pokemonRegistry.entrySet) {
            val mapping = es.value
            val pokemon = Pair(mapping.species, mapping.form)
            for (entry in mapping.workSuitabilityMapping) {
                pokemonMap.getOrPut(pokemon) { mutableListOf() }.add(entry.toPair())
            }
        }
    }

    fun loadedPlugins() = plugins.values

    fun getValidPlugins(block: Identifier, suitability: Identifier) = pluginMap[Pair(block, suitability)].orEmpty()

    fun getWorkSuitability(pokemon: Pokemon): List<Pair<Identifier, Int>> {
        val lookup = Pair(pokemon.species.name, pokemon.form.name)
        return pokemonMap[lookup].orEmpty()
    }

    fun getPlugin(identifier: Identifier) = plugins[identifier]
}