package com.arcaryx.cobblemonworkers.nav

import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import net.minecraft.state.property.DirectionProperty
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction
import kotlin.math.ceil

class FacingAssignmentBlock(private val assignmentBlockFacingDirection: DirectionProperty) :
    AbstractAssignmentStartNavigationConfig() {
    override fun getNavPos(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos): BlockPos {
        val blockState = pokemonEntity.world.getBlockState(assignmentBlockPos)
        val direction = blockState.getOrEmpty(assignmentBlockFacingDirection).orElse(null)
        val offsetBlockPos = if (direction != null) {
            // Calculate the offset based on the entity's width
            val halfEntityWidth = pokemonEntity.width / 2.0
            if (pokemonEntity.width > 1) {
                val facingOffset: Int
                val sideDirection: Direction

                when (direction) {
                    Direction.NORTH -> {
                        sideDirection = Direction.WEST
                        facingOffset = ceil(1 + halfEntityWidth).toInt()
                    }

                    Direction.SOUTH -> {
                        sideDirection = Direction.WEST
                        facingOffset = 1
                    }

                    Direction.WEST -> {
                        sideDirection = Direction.NORTH
                        facingOffset = ceil(1 + halfEntityWidth).toInt()
                    }

                    Direction.EAST -> {
                        sideDirection = Direction.NORTH
                        facingOffset = 1
                    }

                    else -> {
                        sideDirection = Direction.UP
                        facingOffset = 0
                    }
                }
                val sideOffset = halfEntityWidth.toInt()

                val firstOffsetPos = assignmentBlockPos.offset(direction, facingOffset)
                firstOffsetPos.offset(sideDirection, sideOffset)
            } else {
                assignmentBlockPos.offset(direction, 1)
            }
        } else {
            assignmentBlockPos
        }
        // pokemonEntity.world.setBlockState(offsetBlockPos?.down(), Blocks.OBSIDIAN.defaultState)
        return offsetBlockPos
    }
}