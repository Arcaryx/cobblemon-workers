package com.arcaryx.cobblemonworkers.nav

import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import net.minecraft.util.math.BlockPos

class MoveDirectlyToAssignmentBlock() : AbstractAssignmentStartNavigationConfig() {
    override fun getNavPos(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos) = assignmentBlockPos
}