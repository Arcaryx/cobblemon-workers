package com.arcaryx.cobblemonworkers.nav

import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import net.minecraft.util.math.BlockPos

abstract class AbstractAssignmentStartNavigationConfig {
    abstract fun getNavPos(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos): BlockPos
}