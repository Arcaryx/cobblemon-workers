package com.arcaryx.cobblemonworkers.net

import com.cobblemon.mod.common.api.net.NetworkPacket
import com.cobblemon.mod.common.util.cobblemonResource
import net.minecraft.nbt.NbtCompound
import net.minecraft.network.PacketByteBuf
import net.minecraft.stat.Stat
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import java.util.*

class UpdateAssignmentPacket(
    val pokemonUuid: UUID,
    val status: Status,
    val blockPos: BlockPos?,
    val radius: Int?,
    val plugin: Identifier?,
    val config: NbtCompound?) :
    NetworkPacket<UpdateAssignmentPacket> {
    override val id = ID

    enum class Status {
        Create,
        PickBlock,
        PickPlugin,
        Cancel,
        Remove,
        Clear
    }

    override fun encode(buffer: PacketByteBuf) {
        buffer.writeUuid(pokemonUuid)
        buffer.writeEnumConstant(status)
        if (status == Status.PickBlock || status == Status.Remove || status == Status.PickPlugin) {
            buffer.writeBlockPos(blockPos!!)
        }
        if (status == Status.PickBlock || status == Status.PickPlugin) {
            buffer.writeInt(radius!!)
        }
        if (status == Status.PickPlugin) {
            buffer.writeIdentifier(plugin!!)
            buffer.writeNbt(config!!)
        }
    }

    companion object {
        val ID = cobblemonResource("cw_update_assignment")
        fun decode(buffer: PacketByteBuf): UpdateAssignmentPacket {
            val uuid = buffer.readUuid()
            val status = buffer.readEnumConstant(Status::class.java)
            val blockPos = if (status == Status.PickBlock || status == Status.Remove || status == Status.PickPlugin) buffer.readBlockPos() else null
            val radius = if (status == Status.PickBlock || status == Status.PickPlugin) buffer.readInt() else null
            val plugin = if (status == Status.PickPlugin) buffer.readIdentifier() else null
            val config = if (status == Status.PickPlugin) buffer.readNbt() else null
            return UpdateAssignmentPacket(uuid, status, blockPos, radius, plugin, config)
        }
    }
}