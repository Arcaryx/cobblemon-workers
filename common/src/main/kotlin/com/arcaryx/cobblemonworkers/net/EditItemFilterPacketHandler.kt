package com.arcaryx.cobblemonworkers.net

import com.arcaryx.cobblemonworkers.CobblemonWorkers
import com.arcaryx.cobblemonworkers.client.gui.ItemFilterScreenHandler
import com.cobblemon.mod.common.api.net.ServerNetworkPacketHandler
import net.minecraft.server.MinecraftServer
import net.minecraft.server.network.ServerPlayerEntity

object EditItemFilterPacketHandler : ServerNetworkPacketHandler<EditItemFilterPacket> {
    override fun handle(packet: EditItemFilterPacket, server: MinecraftServer, player: ServerPlayerEntity) {
        player.openHandledScreen(CobblemonWorkers.ITEM_FILTER_SCREEN_HANDLER_FACTORY)
        val current = player.currentScreenHandler
        if (current is ItemFilterScreenHandler) {
            current.fieldName = packet.fieldName
            current.setItems(packet.filter)
        }
    }
}