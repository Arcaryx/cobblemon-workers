package com.arcaryx.cobblemonworkers.net

import com.arcaryx.cobblemonworkers.Assignment
import com.cobblemon.mod.common.api.net.NetworkPacket
import com.cobblemon.mod.common.util.cobblemonResource
import net.minecraft.network.PacketByteBuf
import java.util.*

class AssignmentCachePacket(
    val pokemonId: Int,
    val pokemonUuid: UUID,
    val assignments: List<Assignment>
) : NetworkPacket<AssignmentCachePacket> {
    override val id = ID

    override fun encode(buffer: PacketByteBuf) {
        buffer.writeInt(pokemonId)
        buffer.writeUuid(pokemonUuid)
        buffer.writeInt(assignments.size)
        assignments.forEach { it.encode(buffer) }
    }

    companion object {
        val ID = cobblemonResource("cw_assignment_set")
        fun decode(buffer: PacketByteBuf) = AssignmentCachePacket(
            buffer.readInt(),
            buffer.readUuid(),
            List(buffer.readInt()) { Assignment.decode(buffer) }
        )

    }
}