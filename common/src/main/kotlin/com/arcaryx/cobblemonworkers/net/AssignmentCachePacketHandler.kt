package com.arcaryx.cobblemonworkers.net

import com.arcaryx.cobblemonworkers.client.ClientStateManager
import com.cobblemon.mod.common.api.net.ClientNetworkPacketHandler
import net.minecraft.client.MinecraftClient

object AssignmentCachePacketHandler : ClientNetworkPacketHandler<AssignmentCachePacket> {

    override fun handle(packet: AssignmentCachePacket, client: MinecraftClient) {
        ClientStateManager.assigningPokemon = ClientStateManager.AssigningPokemon(packet)
        ClientStateManager.updateEditScreen = true
    }
}