package com.arcaryx.cobblemonworkers.net

import com.arcaryx.cobblemonworkers.CWPokemon
import com.cobblemon.mod.common.api.net.ServerNetworkPacketHandler
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import net.minecraft.server.MinecraftServer
import net.minecraft.server.network.ServerPlayerEntity

object RequestAssignmentCachePacketHandler : ServerNetworkPacketHandler<RequestAssignmentCachePacket> {
    override fun handle(packet: RequestAssignmentCachePacket, server: MinecraftServer, player: ServerPlayerEntity) {
        val entity = player.serverWorld.getEntity(packet.pokemonUuid)
        if (entity != null && entity is PokemonEntity && entity.pokemon.tetheringId != null) {
            val cwPokemon = CWPokemon.fromPokemonEntity(entity)
            cwPokemon?.reconcileAssignments()
            AssignmentCachePacket(entity.id, entity.uuid, cwPokemon?.assignments.orEmpty()).sendToPlayer(player)
        }
    }

}