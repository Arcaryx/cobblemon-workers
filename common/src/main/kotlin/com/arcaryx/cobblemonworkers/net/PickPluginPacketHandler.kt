package com.arcaryx.cobblemonworkers.net

import com.arcaryx.cobblemonworkers.client.ClientStateManager
import com.arcaryx.cobblemonworkers.client.gui.PickPluginScreen
import com.cobblemon.mod.common.api.net.ClientNetworkPacketHandler
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import com.cobblemon.mod.common.util.asTranslated
import net.minecraft.client.MinecraftClient

object PickPluginPacketHandler : ClientNetworkPacketHandler<PickPluginPacket> {
    override fun handle(packet: PickPluginPacket, client: MinecraftClient) {
        if (packet.plugins.isNotEmpty()) {
            PickPluginScreen.open(packet)
        } else {
            val player = MinecraftClient.getInstance().player
            if (player != null) {
                val entity = player.world.getEntityById(ClientStateManager.assigningPokemon!!.id)
                if (entity != null && entity is PokemonEntity) {
                    player.sendMessage(
                        "cobblemonworkers.ui.message.assignment.invalid".asTranslated(entity.getDisplayName()),
                        true
                    )
                }
            }
        }

    }
}