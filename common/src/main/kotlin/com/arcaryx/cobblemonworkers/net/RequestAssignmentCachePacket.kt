package com.arcaryx.cobblemonworkers.net

import com.cobblemon.mod.common.api.net.NetworkPacket
import com.cobblemon.mod.common.util.cobblemonResource
import net.minecraft.network.PacketByteBuf
import java.util.*

class RequestAssignmentCachePacket(val pokemonUuid: UUID) : NetworkPacket<RequestAssignmentCachePacket> {
    override val id = ID

    override fun encode(buffer: PacketByteBuf) {
        buffer.writeUuid(pokemonUuid)
    }

    companion object {
        val ID = cobblemonResource("cw_request_assignment_cache")
        fun decode(buffer: PacketByteBuf) = RequestAssignmentCachePacket(buffer.readUuid())
    }
}