package com.arcaryx.cobblemonworkers.net

import com.arcaryx.cobblemonworkers.CWPokemon
import com.arcaryx.cobblemonworkers.ServerStateManager
import com.arcaryx.cobblemonworkers.plugin.FarmlandConfig
import com.cobblemon.mod.common.api.net.ServerNetworkPacketHandler
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtList
import net.minecraft.server.MinecraftServer
import net.minecraft.server.network.ServerPlayerEntity

object UpdateAssignmentPacketHandler : ServerNetworkPacketHandler<UpdateAssignmentPacket> {
    override fun handle(packet: UpdateAssignmentPacket, server: MinecraftServer, player: ServerPlayerEntity) {
        val pokemonEntity = player.serverWorld.getEntity(packet.pokemonUuid) ?: return
        if (pokemonEntity !is PokemonEntity || pokemonEntity.owner != player || pokemonEntity.tethering == null) {
            return
        }
        when (packet.status) {
            UpdateAssignmentPacket.Status.Create -> {
                ServerStateManager.setAssigningPlayer(player, pokemonEntity.uuid, true)
            }

            UpdateAssignmentPacket.Status.PickBlock -> {
                val validPlugins = ServerStateManager.getSuitablePluginsForBlock(player, packet.blockPos!!)
                PickPluginPacket(pokemonEntity.uuid, packet.blockPos, packet.radius!!, validPlugins.toList()).sendToPlayer(player)
            }

            UpdateAssignmentPacket.Status.PickPlugin -> {
                // TODO: Handle config correctly
                if (ServerStateManager.addAssignmentForBlock(player, packet.blockPos!!, packet.radius!!, packet.plugin!!, FarmlandConfig(packet.config!!))) {
                    ServerStateManager.setAssigningPlayer(player, pokemonEntity.uuid, false)
                    AssignmentPickBlockPacket(pokemonEntity.uuid, true).sendToPlayer(player)
                } else {
                    AssignmentPickBlockPacket(pokemonEntity.uuid, false).sendToPlayer(player)
                }

            }

            UpdateAssignmentPacket.Status.Cancel -> {
                ServerStateManager.setAssigningPlayer(player, pokemonEntity.uuid, false)
            }

            UpdateAssignmentPacket.Status.Remove -> {
                val cwPokemon = CWPokemon.fromPokemonEntity(pokemonEntity)
                cwPokemon?.reconcileAssignments()
                cwPokemon?.removeAssignmentForBlockPos(packet.blockPos!!)
            }

            UpdateAssignmentPacket.Status.Clear -> {
                val cwPokemon = CWPokemon.fromPokemonEntity(pokemonEntity)
                cwPokemon?.clearAssignments()
            }
        }
    }
}