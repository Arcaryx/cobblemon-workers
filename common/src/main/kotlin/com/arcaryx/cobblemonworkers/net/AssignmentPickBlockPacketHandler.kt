package com.arcaryx.cobblemonworkers.net

import com.arcaryx.cobblemonworkers.client.gui.AssignmentEditScreen
import com.arcaryx.cobblemonworkers.client.ClientStateManager
import com.cobblemon.mod.common.api.net.ClientNetworkPacketHandler
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import com.cobblemon.mod.common.util.asTranslated
import net.minecraft.client.MinecraftClient

object AssignmentPickBlockPacketHandler : ClientNetworkPacketHandler<AssignmentPickBlockPacket> {
    override fun handle(packet: AssignmentPickBlockPacket, client: MinecraftClient) {
        if (ClientStateManager.assigningPokemon != null && ClientStateManager.assigningPokemon!!.pokemonUuid == packet.pokemonUuid) {
            if (packet.success) {
                ClientStateManager.assigning = false
                AssignmentEditScreen.open()
            } else {
                val player = MinecraftClient.getInstance().player
                if (player != null) {
                    val entity = player.world.getEntityById(ClientStateManager.assigningPokemon!!.id)
                    if (entity != null && entity is PokemonEntity) {
                        player.sendMessage(
                            "cobblemonworkers.ui.message.assignment.invalid".asTranslated(entity.getDisplayName()),
                            true
                        )
                    }
                }
            }
        }
    }
}