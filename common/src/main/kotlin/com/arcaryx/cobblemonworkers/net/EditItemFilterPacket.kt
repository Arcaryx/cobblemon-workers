package com.arcaryx.cobblemonworkers.net

import com.arcaryx.cobblemonworkers.Assignment
import com.cobblemon.mod.common.api.net.NetworkPacket
import com.cobblemon.mod.common.util.cobblemonResource
import net.minecraft.network.PacketByteBuf
import net.minecraft.util.Identifier
import java.util.*

class EditItemFilterPacket(
    val fieldName: String,
    val filter: List<Identifier>,
) : NetworkPacket<EditItemFilterPacket> {
    override val id = ID

    override fun encode(buffer: PacketByteBuf) {
        buffer.writeString(fieldName)
        buffer.writeInt(filter.size)
        for (item in filter) {
            buffer.writeIdentifier(item)
        }
    }

    companion object {
        val ID = cobblemonResource("cw_edit_item_filter")
        fun decode(buffer: PacketByteBuf) = EditItemFilterPacket(
            buffer.readString(),
            List(buffer.readInt()) { buffer.readIdentifier() }
        )
    }
}