package com.arcaryx.cobblemonworkers.net

import com.cobblemon.mod.common.api.net.NetworkPacket
import com.cobblemon.mod.common.util.cobblemonResource
import net.minecraft.network.PacketByteBuf
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import java.util.*

class PickPluginPacket(val pokemonUuid: UUID, val blockPos: BlockPos, val radius: Int, val plugins: List<Identifier>) : NetworkPacket<PickPluginPacket> {
    override val id = ID

    override fun encode(buffer: PacketByteBuf) {
        buffer.writeUuid(pokemonUuid)
        buffer.writeBlockPos(blockPos)
        buffer.writeInt(radius)
        buffer.writeInt(plugins.size)
        for (plugin in plugins) {
            buffer.writeIdentifier(plugin)
        }
    }

    companion object {
        val ID = cobblemonResource("cw_pick_plugin")
        fun decode(buffer: PacketByteBuf) = PickPluginPacket(
            buffer.readUuid(),
            buffer.readBlockPos(),
            buffer.readInt(),
            (1..buffer.readInt()).map { buffer.readIdentifier() })
    }
}