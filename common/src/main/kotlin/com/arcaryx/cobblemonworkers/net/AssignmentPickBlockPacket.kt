package com.arcaryx.cobblemonworkers.net

import com.cobblemon.mod.common.api.net.NetworkPacket
import com.cobblemon.mod.common.util.cobblemonResource
import net.minecraft.network.PacketByteBuf
import java.util.*

class AssignmentPickBlockPacket(val pokemonUuid: UUID, val success: Boolean) :
    NetworkPacket<AssignmentPickBlockPacket> {
    override val id = ID

    override fun encode(buffer: PacketByteBuf) {
        buffer.writeUuid(pokemonUuid)
        buffer.writeBoolean(success)
    }

    companion object {
        val ID = cobblemonResource("cw_assignment_pick_block")
        fun decode(buffer: PacketByteBuf) = AssignmentPickBlockPacket(buffer.readUuid(), buffer.readBoolean())
    }

}