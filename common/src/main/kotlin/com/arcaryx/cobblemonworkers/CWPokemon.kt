package com.arcaryx.cobblemonworkers

import com.cobblemon.mod.common.block.entity.PokemonPastureBlockEntity
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtElement
import net.minecraft.nbt.NbtHelper
import net.minecraft.nbt.NbtList
import net.minecraft.util.math.BlockPos
import java.util.*

class CWPokemon private constructor(val pokemonEntity: PokemonEntity) {
    val pastureBlockPos: BlockPos
    val tetherUuid: UUID
    val assignments: MutableList<Assignment>

    companion object {
        private const val ASSIGNMENT_PASTURE_BLOCK_POS: String = "cwPastureBlockPos"
        private const val ASSIGNMENT_TETHER: String = "cwTetherUuid"
        private const val ASSIGNMENT_LIST: String = "cwAssignments"

        fun tryCreate(pokemonEntity: PokemonEntity, pastureBlockEntity: PokemonPastureBlockEntity): CWPokemon? {
            if (pokemonEntity.tethering == null) {
                return null
            }
            val data = pokemonEntity.pokemon.persistentData
            data.putUuid(ASSIGNMENT_TETHER, pokemonEntity.tethering!!.tetheringId)
            if (!data.contains(ASSIGNMENT_PASTURE_BLOCK_POS) || NbtHelper.toBlockPos(
                    data.getCompound(
                        ASSIGNMENT_PASTURE_BLOCK_POS
                    )
                ) != pastureBlockEntity.pos
            ) {
                data.put(ASSIGNMENT_PASTURE_BLOCK_POS, NbtHelper.fromBlockPos(pastureBlockEntity.pos))
                data.put(ASSIGNMENT_LIST, NbtList())
            }
            val cwPokemon = CWPokemon(pokemonEntity)
            ServerStateManager.updateAssignmentPokemon(cwPokemon)
            return cwPokemon
        }

        fun fromPokemonEntity(pokemonEntity: PokemonEntity): CWPokemon? {
            val data = pokemonEntity.pokemon.persistentData
            if (pokemonEntity.tethering == null || !data.contains(ASSIGNMENT_PASTURE_BLOCK_POS)) {
                return null
            }
            return CWPokemon(pokemonEntity)
        }
    }

    init {
        val pokemon = pokemonEntity.pokemon
        val data = pokemon.persistentData
        pastureBlockPos = NbtHelper.toBlockPos(data.getCompound(ASSIGNMENT_PASTURE_BLOCK_POS))
        tetherUuid = data.getUuid(ASSIGNMENT_TETHER)
        assignments = toAssignmentList(data.getList(ASSIGNMENT_LIST, NbtElement.COMPOUND_TYPE.toInt()))
    }

    private fun toAssignmentList(assignmentsData: NbtList): MutableList<Assignment> {
        return assignmentsData.map {
            Assignment.fromNbtCompound(it as NbtCompound)
        }.toMutableList()
    }

    private fun savePersistentData() {
        val data = pokemonEntity.pokemon.persistentData
        data.put(ASSIGNMENT_PASTURE_BLOCK_POS, NbtHelper.fromBlockPos(pastureBlockPos))
        data.putUuid(ASSIGNMENT_TETHER, tetherUuid)
        val list = NbtList()
        assignments.forEach { list.add(it.toNbt()) }
        data.put(ASSIGNMENT_LIST, list)
        ServerStateManager.updateAssignmentPokemon(this)
    }

    fun reconcileAssignments() {
        assignments.removeAll { !it.isValid(pokemonEntity.world) }.also { if (it) savePersistentData() }
    }

    fun addAssignment(assignment: Assignment) {
        assignments.add(assignment)
        savePersistentData()
    }

    fun removeAssignment(assignment: Assignment) {
        assignments.remove(assignment)
        savePersistentData()
    }

    fun removeAssignmentForBlockPos(blockPos: BlockPos) {
        assignments.removeIf { it.blockPos == blockPos }.also { if (it) savePersistentData() }
    }

    fun clearAssignments() {
        assignments.clear()
        savePersistentData()
    }
}