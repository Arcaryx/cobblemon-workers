package com.arcaryx.cobblemonworkers.util

import com.cobblemon.mod.common.client.CobblemonClient
import com.cobblemon.mod.common.pokemon.Pokemon
import java.util.*

object ClientUtils {
    fun getPokemonByEntityUuid(pokemonEntityUuid: UUID): Pokemon? {
        return CobblemonClient.storage.pcStores.values.firstNotNullOfOrNull { clientPC ->
            clientPC.boxes.firstNotNullOfOrNull { clientBox ->
                clientBox.slots.find {
                    it?.entity?.uuid == pokemonEntityUuid
                }
            }
        }
    }
}