package com.arcaryx.cobblemonworkers

import com.arcaryx.cobblemonworkers.api.ICWPluginConfig
import com.arcaryx.cobblemonworkers.net.AssignmentCachePacket
import com.cobblemon.mod.common.api.reactive.SimpleObservable
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import com.cobblemon.mod.common.platform.events.PlatformEvents
import net.minecraft.block.BlockState
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.registry.Registries
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World
import java.util.*

object ServerStateManager {
    private val playersAssigningPokemonMap: MutableMap<PlayerEntity, Pair<UUID, Boolean>> = mutableMapOf()
    // Second pair is Pokemon UUID and Assignment UUID TODO: Make this a class
    private val pokemonWorkersUuidMap: MutableMap<Pair<BlockPos, World>, MutableSet<Pair<UUID, UUID>>> = mutableMapOf()

    init {
        PlatformEvents.SERVER_PLAYER_LOGIN.subscribe { event ->
            removeAssigningPlayer(event.player)
        }
    }

    fun setAssigningPlayer(player: PlayerEntity, uuid: UUID, assigning: Boolean) {
        playersAssigningPokemonMap[player] = Pair(uuid, assigning)
    }

    fun removeAssigningPlayer(player: PlayerEntity) = playersAssigningPokemonMap.remove(player)

    fun hasValidWorker(blockPos: BlockPos, blockState: BlockState, world: World): Boolean {
        val blockWorkers = pokemonWorkersUuidMap[Pair(blockPos, world)]
        if (blockWorkers.isNullOrEmpty()) {
            return false
        }

        if (world !is ServerWorld) {
            return false
        }

        blockWorkers.removeIf { uuids ->
            val pokemonEntity = world.getEntity(uuids.first)
            if (pokemonEntity !is PokemonEntity) {
                return@removeIf true
            }
            val cwPokemon = CWPokemon.fromPokemonEntity(pokemonEntity)
            cwPokemon?.reconcileAssignments()
            return@removeIf cwPokemon == null || cwPokemon.assignments.none {
                it.uuid == uuids.second && Registries.BLOCK.getId(blockState.block) == it.block
            }
        }

        return blockWorkers.isNotEmpty()
    }

    fun removeWorker(blockPos: BlockPos, world: World, pokemonEntity: PokemonEntity, assignmentUuuid: UUID) {
        pokemonWorkersUuidMap[Pair(blockPos, world)]?.remove(Pair(pokemonEntity.uuid, assignmentUuuid))
    }

    fun addWorker(blockPos: BlockPos, world: World, pokemonEntity: PokemonEntity, assignmentUuuid: UUID) {
        pokemonWorkersUuidMap.getOrPut(Pair(blockPos, world)) { mutableSetOf() }.add(Pair(pokemonEntity.uuid, assignmentUuuid))
    }

    fun updateAssignmentPokemon(cwPokemon: CWPokemon) {
        val observable = cwPokemon.pokemonEntity.pokemon.getChangeObservable()
        if (observable is SimpleObservable) {
            observable.emit(cwPokemon.pokemonEntity.pokemon)
        }
        val owner = cwPokemon.pokemonEntity.pokemon.getOwnerPlayer()
        if (owner != null && playersAssigningPokemonMap[owner]?.first == cwPokemon.pokemonEntity.uuid) {
            AssignmentCachePacket(
                cwPokemon.pokemonEntity.id,
                cwPokemon.pokemonEntity.uuid,
                cwPokemon.assignments
            ).sendToPlayer(owner)
        }
    }

    fun getSuitablePluginsForBlock(player: ServerPlayerEntity, blockPos: BlockPos): Set<Identifier> {
        val validPlugins = mutableSetOf<Identifier>()
        val assigning = playersAssigningPokemonMap[player] ?: return validPlugins
        val pokemonEntity = player.serverWorld.getEntity(assigning.first)
        if (pokemonEntity == null || pokemonEntity !is PokemonEntity) {
            CobblemonWorkers.LOGGER.warn("PokemonEntity is Null or not PokemonEntity")
            return validPlugins
        }
        val tetheringUuid = pokemonEntity.pokemon.tetheringId
        if (tetheringUuid == null) {
            CobblemonWorkers.LOGGER.warn("tetheringId is Null")
            return validPlugins
        }
        for (suitability in PluginRegistry.getWorkSuitability(pokemonEntity.pokemon)) {
            for (plugin in PluginRegistry.loadedPlugins()) {
                val targetPos = plugin.clickOffset(player.serverWorld, blockPos)
                val blockIdentifier = Registries.BLOCK.getId(pokemonEntity.world.getBlockState(targetPos).block)
                if (PluginRegistry.getValidPlugins(blockIdentifier, suitability.first).contains(plugin)) {
                    validPlugins.add(plugin.getIdentifier())
                }
            }
        }
        return validPlugins
    }


    fun addAssignmentForBlock(player: ServerPlayerEntity, blockPos: BlockPos, radius: Int, plugin: Identifier, config: ICWPluginConfig): Boolean {
        val assigning = playersAssigningPokemonMap[player] ?: return false
        val pokemonEntity = player.serverWorld.getEntity(assigning.first)
        if (pokemonEntity == null || pokemonEntity !is PokemonEntity) {
            CobblemonWorkers.LOGGER.warn("PokemonEntity is Null or not PokemonEntity")
            return false
        }
        val tetheringUuid = pokemonEntity.pokemon.tetheringId
        if (tetheringUuid == null) {
            CobblemonWorkers.LOGGER.warn("tetheringId is Null")
            return false
        }

        var targetPos: BlockPos? = null
        var blockIdentifier: Identifier? = null
        val cwPlugin = PluginRegistry.getPlugin(plugin)?: return false
        for (suitability in PluginRegistry.getWorkSuitability(pokemonEntity.pokemon)) {
            targetPos = cwPlugin.clickOffset(player.serverWorld, blockPos)
            blockIdentifier = Registries.BLOCK.getId(pokemonEntity.world.getBlockState(targetPos).block)
        }

        val cwPokemon = CWPokemon.fromPokemonEntity(pokemonEntity)
        cwPokemon?.reconcileAssignments()
        cwPokemon?.addAssignment(Assignment(UUID.randomUUID(), cwPlugin.getIdentifier(), targetPos!!, blockIdentifier!!, radius, config))
        return true
    }
}