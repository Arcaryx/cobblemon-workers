package com.arcaryx.cobblemonworkers.client.gui

import com.arcaryx.cobblemonworkers.CobblemonWorkers
import com.arcaryx.cobblemonworkers.api.ICWPlugin
import com.arcaryx.cobblemonworkers.api.ICWPluginConfig
import com.arcaryx.cobblemonworkers.gui.GuiFieldType
import com.arcaryx.cobblemonworkers.net.PickPluginPacket
import com.arcaryx.cobblemonworkers.net.UpdateAssignmentPacket
import com.cobblemon.mod.common.CobblemonSounds
import com.cobblemon.mod.common.api.gui.blitk
import com.cobblemon.mod.common.api.text.bold
import com.cobblemon.mod.common.api.text.text
import com.cobblemon.mod.common.client.CobblemonResources
import com.cobblemon.mod.common.client.gui.ExitButton
import com.cobblemon.mod.common.client.render.drawScaledText
import com.cobblemon.mod.common.util.asTranslated
import com.cobblemon.mod.common.util.cobblemonResource
import net.minecraft.client.MinecraftClient
import net.minecraft.client.gui.DrawContext
import net.minecraft.client.gui.screen.Screen
import net.minecraft.client.gui.widget.AlwaysSelectedEntryListWidget
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtElement
import net.minecraft.text.MutableText
import net.minecraft.text.Text
import net.minecraft.util.Identifier

class PluginConfigScreen private constructor(val config: ICWPluginConfig, private val packet: PickPluginPacket, val plugin: ICWPlugin<*>, private val prevScreen: Screen?) :
    AbstractCWScreen(Text.translatable("cobblemonworkers.ui.pluginconfig.title")) {
    private val label: MutableText = "cobblemonworkers.ui.pluginconfig.title".asTranslated()
    private lateinit var pluginConfigList: PluginConfigList


    companion object {
        const val BASE_WIDTH = 136
        const val BASE_HEIGHT = 161

        // Resources
        private val baseResource = Identifier(CobblemonWorkers.MOD_ID, "textures/gui/assignment/assignment_base.png")
        private val buttonResource = cobblemonResource("textures/gui/summary/summary_evolve_select_button.png")

        fun open(config: ICWPluginConfig, packet: PickPluginPacket, plugin: ICWPlugin<*>) {
            val mc = MinecraftClient.getInstance()
            val screen = PluginConfigScreen(config, packet, plugin, mc.currentScreen)
            mc.setScreen(screen)
        }
    }

    override fun init() {
        val x = (width - AssignmentEditScreen.BASE_WIDTH) / 2
        val y = (height - AssignmentEditScreen.BASE_HEIGHT) / 2

        pluginConfigList = PluginConfigList(
            x + 14,
            y + 24,
            config
        )
        addDrawableChild(
            pluginConfigList
        )

        addDrawableChild(
            FixedSummaryButton(
                buttonX = x + 14F,
                buttonY = y + 144F,
                buttonWidth = 40,
                buttonHeight = 10,
                clickAction = {
                    playSound(CobblemonSounds.GUI_CLICK)
                    UpdateAssignmentPacket(
                        packet.pokemonUuid,
                        UpdateAssignmentPacket.Status.PickPlugin,
                        packet.blockPos,
                        packet.radius,
                        plugin.getIdentifier(),
                        config.nbtData
                    ).sendToServer()
                    this.close()
                },
                text = "cobblemonworkers.ui.pluginconfig.confirm".asTranslated(),
                resource = buttonResource,
                boldText = true,
                largeText = false,
                textScale = 0.5F
            )
        )

        addDrawableChild(
            ExitButton(
                pX = x + 96,
                pY = y + 144
            ) {
                playSound(CobblemonSounds.GUI_CLICK)
                this.client?.setScreen(prevScreen)
            }
        )
    }

    override fun render(context: DrawContext, mouseX: Int, mouseY: Int, delta: Float) {

        val x = (width - BASE_WIDTH) / 2
        val y = (height - BASE_HEIGHT) / 2
        val matrices = context.matrices

        // Render Base Resource (Screen Middle)
        blitk(
            matrixStack = matrices,
            texture = baseResource,
            x = x,
            y = y,
            width = BASE_WIDTH,
            height = BASE_HEIGHT
        )

        // Label
        drawScaledText(
            context = context,
            font = CobblemonResources.DEFAULT_LARGE,
            text = label.bold(),
            x = x + BASE_WIDTH / 2,
            y = y + 2,
            centered = true,
            shadow = true
        )

        drawScaledText(
            context = context,
            font = CobblemonResources.DEFAULT_LARGE,
            text = "WIP Menu (to be redrawn)".text(),
            x = x + BASE_WIDTH / 2,
            y = y + 15,
            centered = true,
            shadow = true,
            scale = 0.8F
        )

        super.render(context, mouseX, mouseY, delta)
    }

    fun setConfigValue(name: String, data: NbtElement) {
        config.nbtData.put(name, data)
        pluginConfigList.refresh()
    }

    class PluginConfigList(
        val x: Int,
        val y: Int,
        val config: ICWPluginConfig,
    ) : AlwaysSelectedEntryListWidget<ConfigEntry>(
        MinecraftClient.getInstance(),
        WIDTH, // width
        HEIGHT, // height
        0, // top
        HEIGHT, // bottom
        SLOT_HEIGHT + SLOT_SPACING
    ) {
        companion object {
            const val WIDTH = 108
            const val HEIGHT = 114 // Why not 118?
            const val SLOT_WIDTH = 91

            const val SLOT_HEIGHT = 25
            const val SLOT_SPACING = 5

            private val backgroundResource = cobblemonResource("textures/gui/summary/summary_scroll_background.png")
            private val scrollOverlayResource = cobblemonResource("textures/gui/summary/summary_scroll_overlay.png")
        }

        private var scrolling = false
        private var entriesCreated = false

        init {
            correctSize()
            setRenderHorizontalShadows(false)
            setRenderBackground(false)
            setRenderSelection(false)
        }

        fun refresh() {
            clearEntries()
            entriesCreated = false
        }

        override fun getRowWidth(): Int {
            return SLOT_WIDTH
        }

        override fun getScrollbarPositionX(): Int {
            return left + width - 3
        }

        override fun render(context: DrawContext, mouseX: Int, mouseY: Int, partialTicks: Float) {
            if (!entriesCreated) {
                entriesCreated = true
                for (field in config.getGuiFields()) {
                    when(field.type){
                        GuiFieldType.TYPE_CHECKBOX -> addEntry(ConfigEntryCheckbox(
                            field, config
                        ))
                        GuiFieldType.TYPE_INTEGER -> continue // TODO
                        GuiFieldType.TYPE_ITEM_FILTER -> addEntry(ConfigEntryItemFilter(
                            field, config
                        ))
                        GuiFieldType.TYPE_BLOCK_FILTER -> continue // TODO
                    }
                }

            }

            val matrices = context.matrices
            correctSize()
            blitk(
                matrixStack = matrices,
                texture = backgroundResource,
                x = left,
                y = top,
                height = PickPluginScreen.PickPluginList.HEIGHT,
                width = PickPluginScreen.PickPluginList.WIDTH
            )

            super.render(context, mouseX, mouseY, partialTicks)

            // Scroll Overlay
            val scrollOverlayOffset = 4
            blitk(
                matrixStack = matrices,
                texture = scrollOverlayResource,
                x = left,
                y = top - (scrollOverlayOffset / 2),
                height = PickPluginScreen.PickPluginList.HEIGHT + scrollOverlayOffset,
                width = PickPluginScreen.PickPluginList.WIDTH
            )
        }

        override fun mouseClicked(mouseX: Double, mouseY: Double, button: Int): Boolean {
            updateScrollingState(mouseX, mouseY)
            if (scrolling) {
                focused = getEntryAtPosition(mouseX, mouseY)
                isDragging = true
            }
            return super.mouseClicked(mouseX, mouseY, button)
        }

        override fun mouseDragged(
            mouseX: Double,
            mouseY: Double,
            button: Int,
            deltaX: Double,
            deltaY: Double
        ): Boolean {
            if (scrolling) {
                if (mouseY < top) {
                    setScrollAmount(0.0)
                } else if (mouseY > bottom) {
                    setScrollAmount(maxScroll.toDouble())
                } else {
                    setScrollAmount(scrollAmount + deltaY)
                }
            }
            return super.mouseDragged(mouseX, mouseY, button, deltaX, deltaY)
        }

        private fun updateScrollingState(mouseX: Double, mouseY: Double) {
            scrolling = mouseX >= this.scrollbarPositionX.toDouble()
                    && mouseX < (this.scrollbarPositionX + 3).toDouble()
                    && mouseY >= top
                    && mouseY < bottom
        }

        private fun correctSize() {
            updateSize(
                WIDTH,
                HEIGHT, y + 1, (y + 1) + (HEIGHT - 2))
            setLeftPos(x)
        }
    }
}