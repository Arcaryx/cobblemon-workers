package com.arcaryx.cobblemonworkers.client

import com.arcaryx.cobblemonworkers.Assignment
import com.arcaryx.cobblemonworkers.client.gui.AssignmentEditScreen
import com.arcaryx.cobblemonworkers.net.AssignmentCachePacket
import com.arcaryx.cobblemonworkers.net.UpdateAssignmentPacket
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import com.cobblemon.mod.common.platform.events.PlatformEvents
import com.cobblemon.mod.common.util.asTranslated
import net.minecraft.client.MinecraftClient
import net.minecraft.client.network.ClientPlayerEntity
import net.minecraft.util.math.BlockPos
import java.util.*
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sign

object ClientStateManager {
    var assigningPokemon: AssigningPokemon? = null
    var updateEditScreen = false
    var assigning = false
    var radius = 1

    init {
        PlatformEvents.CLIENT_PLAYER_LOGIN.subscribe {
            assigningPokemon = null
            assigning = false
            radius = 1
        }
    }

    class AssigningPokemon(packet: AssignmentCachePacket) {
        val id: Int = packet.pokemonId
        val pokemonUuid: UUID = packet.pokemonUuid
        val assignments: List<Assignment> = packet.assignments
    }

    fun startAssigning() {
        val player = MinecraftClient.getInstance().player
        if (assigningPokemon != null && player != null && player.world != null) {
            val pokemon = player.world.getEntityById(assigningPokemon!!.id)
            if (pokemon != null && pokemon is PokemonEntity) {
                UpdateAssignmentPacket(
                    assigningPokemon!!.pokemonUuid,
                    UpdateAssignmentPacket.Status.Create,
                    null,
                    null,
                    null,
                    null
                ).sendToServer()
                player.sendMessage(
                    "cobblemonworkers.ui.message.set.assignment".asTranslated(pokemon.getDisplayName()),
                    true
                )
            }
        }
        assigning = true
        radius = 0
    }

    fun doAttack(player: ClientPlayerEntity): Boolean {
        if (!assigning || assigningPokemon == null) {
            return false
        }
        val pokemonEntity = player.clientWorld.getEntityById(assigningPokemon!!.id) ?: return false
        if (pokemonEntity is PokemonEntity) {
            UpdateAssignmentPacket(
                assigningPokemon!!.pokemonUuid,
                UpdateAssignmentPacket.Status.Cancel,
                null,
                null,
                null,
                null
            ).sendToServer()
            AssignmentEditScreen.open()
        }
        assigning = false
        return true
    }

    fun doInteractWithBlock(player: ClientPlayerEntity, blockPos: BlockPos): Boolean {
        if (!assigning || assigningPokemon == null) {
            return false
        }
        val pokemonEntity = player.clientWorld.getEntityById(assigningPokemon!!.id) ?: return false
        if (pokemonEntity is PokemonEntity) {
            UpdateAssignmentPacket(
                assigningPokemon!!.pokemonUuid,
                UpdateAssignmentPacket.Status.PickBlock,
                blockPos,
                radius,
                null,
                null
            ).sendToServer()
        }
        return true
    }

    fun adjustRadius(scrollAmount: Double): Boolean {
        if (!assigning || assigningPokemon == null) {
            return false
        }

        val i = sign(scrollAmount).toInt()
        radius = if (i > 0) {
            min(radius + 1, 9) // TODO: Config this
        } else {
            max(radius - 1, 0)
        }

        val player = MinecraftClient.getInstance().player
        player?.sendMessage("cobblemonworkers.ui.message.assignment.radius".asTranslated(radius), true)

        return true
    }
}