package com.arcaryx.cobblemonworkers.client.gui

import com.arcaryx.cobblemonworkers.api.ICWPluginConfig
import com.arcaryx.cobblemonworkers.gui.GuiField
import com.cobblemon.mod.common.api.gui.blitk
import com.cobblemon.mod.common.util.cobblemonResource
import net.minecraft.client.MinecraftClient
import net.minecraft.client.gui.DrawContext
import net.minecraft.client.gui.widget.AlwaysSelectedEntryListWidget
import net.minecraft.client.sound.PositionedSoundInstance
import net.minecraft.sound.SoundEvent

abstract class ConfigEntry(val field: GuiField, val config: ICWPluginConfig) : AlwaysSelectedEntryListWidget.Entry<ConfigEntry>() {
    companion object {
        private val slotResource = cobblemonResource("textures/gui/summary/summary_evolve_slot.png")
    }

    override fun render(
        context: DrawContext,
        index: Int,
        rowTop: Int,
        rowLeft: Int,
        rowWidth: Int,
        rowHeight: Int,
        mouseX: Int,
        mouseY: Int,
        isHovered: Boolean,
        partialTicks: Float
    ) {
        val x = rowLeft - 3
        val y = rowTop
        val matrices = context.matrices

        blitk(
            matrixStack = matrices,
            texture = slotResource,
            x = x,
            y = y,
            height = PickPluginScreen.PickPluginList.SLOT_HEIGHT,
            width = rowWidth
        )
    }

    protected fun playSound(soundEvent: SoundEvent) {
        MinecraftClient.getInstance().soundManager.play(PositionedSoundInstance.master(soundEvent, 1.0F))
    }

}