package com.arcaryx.cobblemonworkers.client.gui

import com.arcaryx.cobblemonworkers.CobblemonWorkers
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.inventory.Inventory
import net.minecraft.inventory.SimpleInventory
import net.minecraft.item.ItemStack
import net.minecraft.registry.Registries
import net.minecraft.screen.ScreenHandler
import net.minecraft.screen.slot.Slot
import net.minecraft.util.Identifier

class ItemFilterScreenHandler(syncId: Int, playerInventory: PlayerInventory) : ScreenHandler(CobblemonWorkers.ITEM_FILTER_SCREEN_HANDLER, syncId) {

    private val slotCount: Int = 5
    private val inventory: Inventory = SimpleInventory(slotCount)
    var fieldName: String = ""

    init {
        inventory.onOpen(playerInventory.player)

        // TODO: Clean up below (Copied from mc java class)
        var j = 0
        while (j < 5) {
            this.addSlot(Slot(inventory, j, 44 + j * 18, 20))
            ++j
        }

        j = 0
        while (j < 3) {
            for (k in 0..8) {
                this.addSlot(Slot(playerInventory, k + j * 9 + 9, 8 + k * 18, j * 18 + 51))
            }
            ++j
        }

        j = 0
        while (j < 9) {
            this.addSlot(Slot(playerInventory, j, 8 + j * 18, 109))
            ++j
        }


    }

    fun setItems(items: List<Identifier>) {
        for (i in 0 until slotCount) {
            if (items.size >= i) {
                //this.inserti
                //slots[i].set = Registries.ITEM.get(items.get(i)).defaultStack
                // TODO: Populate initial
            }
        }
    }

    override fun quickMove(player: PlayerEntity, slot: Int): ItemStack {
        // TODO: Clean up with new logic (from hopper)
        var itemStack = ItemStack.EMPTY
        val slot2 = slots[slot]
        if (slot2.hasStack()) {
            val itemStack2 = slot2.stack
            itemStack = itemStack2.copy()
            if (slot < inventory.size()) {
                if (!this.insertItem(
                        itemStack2,
                        inventory.size(), slots.size, true
                    )
                ) {
                    return ItemStack.EMPTY
                }
            } else if (!this.insertItem(itemStack2, 0, inventory.size(), false)) {
                return ItemStack.EMPTY
            }

            if (itemStack2.isEmpty) {
                slot2.stack = ItemStack.EMPTY
            } else {
                slot2.markDirty()
            }
        }

        return itemStack!!
    }

    override fun canUse(player: PlayerEntity): Boolean {
        return inventory.canPlayerUse(player)
    }

    override fun onClosed(player: PlayerEntity) {
        super.onClosed(player)
        inventory.onClose(player)
    }
}