package com.arcaryx.cobblemonworkers.client.gui

import net.minecraft.client.MinecraftClient
import net.minecraft.client.gui.DrawContext
import net.minecraft.client.gui.screen.Screen
import net.minecraft.client.gui.screen.ingame.HandledScreen
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.nbt.NbtList
import net.minecraft.nbt.NbtString
import net.minecraft.registry.Registries
import net.minecraft.text.Text
import net.minecraft.util.Identifier

class ItemFilterScreen(handler: ItemFilterScreenHandler, inventory: PlayerInventory, title: Text) : HandledScreen<ItemFilterScreenHandler>(handler, inventory, title) {

    private val prevScreen: Screen? = MinecraftClient.getInstance().currentScreen

    companion object {
        val NAME = "item_filter"
        val TITLE = Text.translatable("cobblemonworkers.ui.item_filter.title")
    }

    init {
        this.backgroundHeight = 133
        this.playerInventoryTitleY = this.backgroundHeight - 94
    }

    // Resources
    private val baseResource = Identifier("textures/gui/container/hopper.png")

    override fun render(context: DrawContext?, mouseX: Int, mouseY: Int, delta: Float) {
        this.renderBackground(context)
        super.render(context, mouseX, mouseY, delta)
        this.drawMouseoverTooltip(context, mouseX, mouseY)
    }

    override fun drawBackground(context: DrawContext?, delta: Float, mouseX: Int, mouseY: Int) {
        val x = (this.width - this.backgroundWidth) / 2
        val y = (this.height - this.backgroundHeight) / 2
        context!!.drawTexture(baseResource, x, y, 0, 0, this.backgroundWidth, this.backgroundHeight)
    }

    override fun close() {
        if (prevScreen is PluginConfigScreen) {
            val nbtList = NbtList()
            nbtList.addAll(handler.slots.filterNotNull().map {
                NbtString.of(Registries.ITEM.getId(it.stack.item).toString())
            })
            prevScreen.setConfigValue(handler.fieldName, nbtList)
            MinecraftClient.getInstance().setScreen(prevScreen)
        }
    }


}