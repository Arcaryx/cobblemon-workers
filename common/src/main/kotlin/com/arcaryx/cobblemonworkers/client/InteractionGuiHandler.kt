package com.arcaryx.cobblemonworkers.client

import com.arcaryx.cobblemonworkers.client.gui.AssignmentEditScreen
import com.arcaryx.cobblemonworkers.net.RequestAssignmentCachePacket
import com.arcaryx.cobblemonworkers.util.ClientUtils
import com.cobblemon.mod.common.api.events.CobblemonEvents
import com.cobblemon.mod.common.client.gui.interact.wheel.InteractWheelOption
import com.cobblemon.mod.common.client.gui.interact.wheel.Orientation
import com.cobblemon.mod.common.util.cobblemonResource

object InteractionGuiHandler {
    fun init() {
        CobblemonEvents.POKEMON_INTERACTION_GUI_CREATION.subscribe { event ->
            val pokemon = ClientUtils.getPokemonByEntityUuid(event.pokemonID)
            RequestAssignmentCachePacket(event.pokemonID).sendToServer()
            pokemon?.tetheringId?.let {
                val setAssigment = InteractWheelOption(
                    // TODO: New Icon
                    iconResource = cobblemonResource("textures/gui/interact/icon_held_item.png"),
                    tooltipText = "cobblemonworkers.ui.interact.assignments",
                    onPress = {
                        AssignmentEditScreen.open()
                    }
                )
                event.options.put(Orientation.TOP_LEFT, setAssigment)
            }
        }
    }
}