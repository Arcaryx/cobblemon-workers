package com.arcaryx.cobblemonworkers.client.gui

import com.arcaryx.cobblemonworkers.CobblemonWorkers
import com.arcaryx.cobblemonworkers.api.ICWPluginConfig
import com.arcaryx.cobblemonworkers.gui.GuiField
import com.cobblemon.mod.common.CobblemonSounds
import com.cobblemon.mod.common.client.CobblemonResources
import com.cobblemon.mod.common.client.gui.summary.SummaryButton
import com.cobblemon.mod.common.client.render.drawScaledText
import com.cobblemon.mod.common.util.asTranslated
import com.cobblemon.mod.common.util.cobblemonResource
import net.minecraft.client.gui.DrawContext
import net.minecraft.text.Text

class ConfigEntryCheckbox(field: GuiField, config: ICWPluginConfig) : ConfigEntry(field, config) {

    private val buttonResource = cobblemonResource("textures/gui/summary/summary_evolve_select_button.png")

    private val toggleButton = SummaryButton(
        buttonX = 0F,
        buttonY = 0F,
        buttonWidth = 40,
        buttonHeight = 10,
        clickAction = {
            playSound(CobblemonSounds.GUI_CLICK)
            config.nbtData.putBoolean(field.name, !config.nbtData.getBoolean(field.name))
        },
        text = "cobblemonworkers.ui.button.toggle".asTranslated(),
        resource = buttonResource,
        boldText = true,
        largeText = false,
        textScale = 0.5F
    )

    override fun render(
        context: DrawContext,
        index: Int,
        rowTop: Int,
        rowLeft: Int,
        rowWidth: Int,
        rowHeight: Int,
        mouseX: Int,
        mouseY: Int,
        isHovered: Boolean,
        partialTicks: Float
    ) {
        super.render(context, index, rowTop, rowLeft, rowWidth, rowHeight, mouseX, mouseY, isHovered, partialTicks)

        val x = rowLeft - 3
        val y = rowTop
        val matrices = context.matrices

        toggleButton.setPosFloat(x + 23F, y + 13F)
        toggleButton.render(context, mouseX, mouseY, partialTicks)

        drawScaledText(
            context = context,
            font = CobblemonResources.DEFAULT_LARGE,
            text = "${CobblemonWorkers.MOD_ID}.cwplugin.config.${field.name}".asTranslated(),
            x = x + 4,
            y = y + 2,
            shadow = true
        )

        // TODO: Replace with checkbox?
        drawScaledText(
            context = context,
            font = CobblemonResources.DEFAULT_LARGE,
            text = Text.literal(config.nbtData.getBoolean(field.name).toString()),
            x = x + 70,
            y = y + 5,
            shadow = true
        )
    }

    override fun mouseClicked(d: Double, e: Double, i: Int): Boolean {
        if (toggleButton.isHovered) {
            toggleButton.onPress()
            return true
        }
        return false
    }

    override fun getNarration(): Text {
        return Text.empty()
    }


}