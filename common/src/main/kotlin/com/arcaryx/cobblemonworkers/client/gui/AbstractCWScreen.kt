package com.arcaryx.cobblemonworkers.client.gui

import net.minecraft.client.MinecraftClient
import net.minecraft.client.gui.screen.Screen
import net.minecraft.client.sound.PositionedSoundInstance
import net.minecraft.sound.SoundEvent
import net.minecraft.text.Text


abstract class AbstractCWScreen(
    title: Text
) : Screen(title) {

    override fun init() {}

    companion object {
        fun playSound(soundEvent: SoundEvent) {
            MinecraftClient.getInstance().soundManager.play(PositionedSoundInstance.master(soundEvent, 1.0F))
        }
    }

    override fun shouldPause(): Boolean = false

    override fun mouseScrolled(mouseX: Double, mouseY: Double, amount: Double): Boolean {
        return children().any { it.mouseScrolled(mouseX, mouseY, amount) }
    }

    override fun mouseClicked(mouseX: Double, mouseY: Double, button: Int): Boolean {
        val test = children().any { it.mouseClicked(mouseX, mouseY, button) }
        return test
    }
}
