package com.arcaryx.cobblemonworkers.client.gui

import com.arcaryx.cobblemonworkers.CobblemonWorkers
import com.arcaryx.cobblemonworkers.api.ICWPluginConfig
import com.arcaryx.cobblemonworkers.gui.GuiField
import com.arcaryx.cobblemonworkers.net.EditItemFilterPacket
import com.cobblemon.mod.common.CobblemonSounds
import com.cobblemon.mod.common.client.CobblemonResources
import com.cobblemon.mod.common.client.gui.summary.SummaryButton
import com.cobblemon.mod.common.client.render.drawScaledText
import com.cobblemon.mod.common.util.asTranslated
import com.cobblemon.mod.common.util.cobblemonResource
import net.minecraft.client.MinecraftClient
import net.minecraft.client.gui.DrawContext
import net.minecraft.item.Items
import net.minecraft.nbt.NbtElement
import net.minecraft.registry.Registries
import net.minecraft.text.Text
import net.minecraft.util.Identifier

class ConfigEntryItemFilter(field: GuiField, config: ICWPluginConfig) : ConfigEntry(field, config) {

    private val buttonResource = cobblemonResource("textures/gui/summary/summary_evolve_select_button.png")

    private val editButton = SummaryButton(
        buttonX = 0F,
        buttonY = 0F,
        buttonWidth = 40,
        buttonHeight = 10,
        clickAction = {
            playSound(CobblemonSounds.GUI_CLICK)
            val itemList = config.nbtData.getList(field.name, NbtElement.STRING_TYPE.toInt()).map {
                Identifier(it.asString())
            }
            EditItemFilterPacket(field.name, itemList).sendToServer()
        },
        text = "cobblemonworkers.ui.button.edit".asTranslated(),
        resource = buttonResource,
        boldText = true,
        largeText = false,
        textScale = 0.5F
    )

    override fun render(
        context: DrawContext,
        index: Int,
        rowTop: Int,
        rowLeft: Int,
        rowWidth: Int,
        rowHeight: Int,
        mouseX: Int,
        mouseY: Int,
        isHovered: Boolean,
        partialTicks: Float
    ) {
        super.render(context, index, rowTop, rowLeft, rowWidth, rowHeight, mouseX, mouseY, isHovered, partialTicks)

        val x = rowLeft - 3
        val y = rowTop
        val matrices = context.matrices

        editButton.setPosFloat(x + 23F, y + 13F)
        editButton.render(context, mouseX, mouseY, partialTicks)

        drawScaledText(
            context = context,
            font = CobblemonResources.DEFAULT_LARGE,
            text = "${CobblemonWorkers.MOD_ID}.cwplugin.config.${field.name}".asTranslated(),
            x = x + 4,
            y = y + 2,
            shadow = true
        )

        val itemList = config.nbtData.getList(field.name, NbtElement.STRING_TYPE.toInt()).map {
            Identifier(it.asString())
        }
        val ticksPerItem = 10
        var item = Items.BARRIER.asItem().defaultStack
        if (itemList.isNotEmpty()) {
            val itemIdentifier = itemList[((partialTicks / ticksPerItem).toInt() % itemList.size)]
            item = Registries.ITEM.get(itemIdentifier).asItem().defaultStack
        }
        val blockX = x + 70
        val blockY = y + 5
        if (!item.isEmpty) {
            context.drawItem(item, blockX, blockY)
            //context.drawItemInSlot(MinecraftClient.getInstance().textRenderer, blockItem, itemX, itemY)
        }
    }

    override fun mouseClicked(d: Double, e: Double, i: Int): Boolean {
        if (editButton.isHovered) {
            editButton.onPress()
            return true
        }
        return false
    }

    override fun getNarration(): Text {
        return Text.empty()
    }
}