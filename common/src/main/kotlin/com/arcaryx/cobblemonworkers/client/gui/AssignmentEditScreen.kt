package com.arcaryx.cobblemonworkers.client.gui

import com.arcaryx.cobblemonworkers.Assignment
import com.arcaryx.cobblemonworkers.CobblemonWorkers
import com.arcaryx.cobblemonworkers.client.ClientStateManager
import com.arcaryx.cobblemonworkers.net.UpdateAssignmentPacket
import com.cobblemon.mod.common.CobblemonSounds
import com.cobblemon.mod.common.api.gui.blitk
import com.cobblemon.mod.common.api.text.bold
import com.cobblemon.mod.common.api.text.text
import com.cobblemon.mod.common.client.CobblemonResources
import com.cobblemon.mod.common.client.gui.ExitButton
import com.cobblemon.mod.common.client.gui.summary.SummaryButton
import com.cobblemon.mod.common.client.render.drawScaledText
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import com.cobblemon.mod.common.util.asTranslated
import com.cobblemon.mod.common.util.cobblemonResource
import net.minecraft.client.MinecraftClient
import net.minecraft.client.gui.DrawContext
import net.minecraft.client.gui.widget.AlwaysSelectedEntryListWidget
import net.minecraft.registry.Registries
import net.minecraft.text.MutableText
import net.minecraft.text.Text
import net.minecraft.util.Identifier


class AssignmentEditScreen private constructor(pokemonEntity: PokemonEntity) :
    AbstractCWScreen(Text.translatable("cobblemonworkers.ui.assignmentedit.title")) {
    private val label: MutableText = "cobblemonworkers.ui.assignments".asTranslated(pokemonEntity.displayName)

    companion object {
        const val BASE_WIDTH = 136
        const val BASE_HEIGHT = 161

        // Resources
        private val baseResource = Identifier(CobblemonWorkers.MOD_ID, "textures/gui/assignment/assignment_base.png")
        private val buttonResource = cobblemonResource("textures/gui/summary/summary_evolve_select_button.png")

        fun open() {
            val mc = MinecraftClient.getInstance()
            if (ClientStateManager.assigningPokemon != null && mc.world != null) {
                val entity = mc.world!!.getEntityById(ClientStateManager.assigningPokemon!!.id)
                if (entity != null && entity is PokemonEntity) {
                    val screen = AssignmentEditScreen(entity)
                    mc.setScreen(screen)
                }
            }
        }
    }

    override fun init() {
        val x = (width - BASE_WIDTH) / 2
        val y = (height - BASE_HEIGHT) / 2

        addDrawableChild(
            AssignmentEditList(
                x + 14,
                y + 24
            )
        )

        addDrawableChild(
            FixedSummaryButton(
                buttonX = x + 14F,
                buttonY = y + 144F,
                buttonWidth = 40,
                buttonHeight = 10,
                clickAction = {
                    ClientStateManager.startAssigning()
                    this.close()
                },
                text = "cobblemonworkers.ui.assignments.add".asTranslated(),
                resource = buttonResource,
                boldText = true,
                largeText = false,
                textScale = 0.5F
            )
        )

        addDrawableChild(
            FixedSummaryButton(
                buttonX = x + 55F,
                buttonY = y + 144F,
                buttonWidth = 40,
                buttonHeight = 10,
                clickAction = {
                    if (ClientStateManager.assigningPokemon != null) {
                        UpdateAssignmentPacket(
                            ClientStateManager.assigningPokemon!!.pokemonUuid,
                            UpdateAssignmentPacket.Status.Clear,
                            null,
                            null,
                            null,
                            null
                        ).sendToServer()
                    }
                },
                text = "cobblemonworkers.ui.assignments.clear".asTranslated(),
                resource = buttonResource,
                boldText = true,
                largeText = false,
                textScale = 0.5F
            )
        )

        addDrawableChild(
            ExitButton(
                pX = x + 96,
                pY = y + 144
            ) {
                playSound(CobblemonSounds.GUI_CLICK)
                this.close()
            }
        )

    }

    override fun render(context: DrawContext, mouseX: Int, mouseY: Int, delta: Float) {

        val x = (width - BASE_WIDTH) / 2
        val y = (height - BASE_HEIGHT) / 2
        val matrices = context.matrices

        // Render Base Resource (Screen Middle)
        blitk(
            matrixStack = matrices,
            texture = baseResource,
            x = x,
            y = y,
            width = BASE_WIDTH,
            height = BASE_HEIGHT
        )

        // Label
        drawScaledText(
            context = context,
            font = CobblemonResources.DEFAULT_LARGE,
            text = label.bold(),
            x = x + BASE_WIDTH / 2,
            y = y + 2,
            centered = true,
            shadow = true
        )

        drawScaledText(
            context = context,
            font = CobblemonResources.DEFAULT_LARGE,
            text = "WIP Menu (to be redrawn)".text(),
            x = x + BASE_WIDTH / 2,
            y = y + 15,
            centered = true,
            shadow = true,
            scale = 0.8F
        )

        super.render(context, mouseX, mouseY, delta)
    }

    class AssignmentEditList(
        val x: Int,
        val y: Int,
    ) : AlwaysSelectedEntryListWidget<AssignmentEditList.AssignmentSlot>(
        MinecraftClient.getInstance(),
        WIDTH, // width
        HEIGHT, // height
        0, // top
        HEIGHT, // bottom
        SLOT_HEIGHT + SLOT_SPACING
    ) {
        companion object {
            const val WIDTH = 108
            const val HEIGHT = 114 // Why not 118?
            const val SLOT_WIDTH = 91

            const val SLOT_HEIGHT = 25
            const val SLOT_SPACING = 5

            private val backgroundResource = cobblemonResource("textures/gui/summary/summary_scroll_background.png")
            private val scrollOverlayResource = cobblemonResource("textures/gui/summary/summary_scroll_overlay.png")
            private val slotResource = cobblemonResource("textures/gui/summary/summary_evolve_slot.png")
        }

        private var scrolling = false
        private var entriesCreated = false

        init {
            correctSize()
            setRenderHorizontalShadows(false)
            setRenderBackground(false)
            setRenderSelection(false)
        }

        override fun getRowWidth(): Int {
            return SLOT_WIDTH
        }

        override fun getScrollbarPositionX(): Int {
            return left + width - 3
        }

        override fun render(context: DrawContext, mouseX: Int, mouseY: Int, partialTicks: Float) {
            if ((!entriesCreated || ClientStateManager.updateEditScreen) && ClientStateManager.assigningPokemon != null) {
                this.clearEntries()
                entriesCreated = true
                ClientStateManager.assigningPokemon!!.assignments.map {
                    AssignmentSlot(
                        it,
                        this.left,
                        this.top,
                        this.right,
                        this.bottom
                    )
                }.forEach { entry -> this.addEntry(entry) }
                ClientStateManager.updateEditScreen = false
            }

            val matrices = context.matrices
            correctSize()
            blitk(
                matrixStack = matrices,
                texture = backgroundResource,
                x = left,
                y = top,
                height = HEIGHT,
                width = WIDTH
            )

            super.render(context, mouseX, mouseY, partialTicks)

            // Scroll Overlay
            val scrollOverlayOffset = 4
            blitk(
                matrixStack = matrices,
                texture = scrollOverlayResource,
                x = left,
                y = top - (scrollOverlayOffset / 2),
                height = HEIGHT + scrollOverlayOffset,
                width = WIDTH
            )
        }

        override fun mouseClicked(mouseX: Double, mouseY: Double, button: Int): Boolean {
            updateScrollingState(mouseX, mouseY)
            if (scrolling) {
                focused = getEntryAtPosition(mouseX, mouseY)
                isDragging = true
            }
            return super.mouseClicked(mouseX, mouseY, button)
        }

        override fun mouseDragged(
            mouseX: Double,
            mouseY: Double,
            button: Int,
            deltaX: Double,
            deltaY: Double
        ): Boolean {
            if (scrolling) {
                if (mouseY < top) {
                    setScrollAmount(0.0)
                } else if (mouseY > bottom) {
                    setScrollAmount(maxScroll.toDouble())
                } else {
                    setScrollAmount(scrollAmount + deltaY)
                }
            }
            return super.mouseDragged(mouseX, mouseY, button, deltaX, deltaY)
        }

        private fun updateScrollingState(mouseX: Double, mouseY: Double) {
            scrolling = mouseX >= this.scrollbarPositionX.toDouble()
                    && mouseX < (this.scrollbarPositionX + 3).toDouble()
                    && mouseY >= top
                    && mouseY < bottom
        }

        private fun correctSize() {
            updateSize(WIDTH, HEIGHT, y + 1, (y + 1) + (HEIGHT - 2))
            setLeftPos(x)
        }

        class AssignmentSlot(
            val assignment: Assignment,
            val a: Int,
            val b: Int,
            val c: Int,
            val d: Int
        ) : Entry<AssignmentSlot>() {

            private val removeButton = SummaryButton(
                buttonX = 0F,
                buttonY = 0F,
                buttonWidth = 40,
                buttonHeight = 10,
                clickAction = {
                    playSound(CobblemonSounds.GUI_CLICK)
                    if (ClientStateManager.assigningPokemon != null) {
                        UpdateAssignmentPacket(
                            ClientStateManager.assigningPokemon!!.pokemonUuid,
                            UpdateAssignmentPacket.Status.Remove,
                            assignment.blockPos,
                            null,
                            null,
                            null
                        ).sendToServer()
                    }
                },
                text = "cobblemonworkers.ui.assignments.remove".asTranslated(),
                resource = buttonResource,
                boldText = true,
                largeText = false,
                textScale = 0.5F
            )

            override fun render(
                context: DrawContext,
                index: Int,
                rowTop: Int,
                rowLeft: Int,
                rowWidth: Int,
                rowHeight: Int,
                mouseX: Int,
                mouseY: Int,
                isHovered: Boolean,
                partialTicks: Float
            ) {
                val x = rowLeft - 3
                val y = rowTop
                val matrices = context.matrices

                blitk(
                    matrixStack = matrices,
                    texture = slotResource,
                    x = x,
                    y = y,
                    height = SLOT_HEIGHT,
                    width = rowWidth
                )

                // TODO: Rescale bigger?
                val blockItem = Registries.BLOCK.get(assignment.block).asItem().defaultStack
                val blockX = x + 70
                val blockY = y + 5
                if (!blockItem.isEmpty) {
                    context.drawItem(blockItem, blockX, blockY)
                    //context.drawItemInSlot(MinecraftClient.getInstance().textRenderer, blockItem, itemX, itemY)
                }

                drawScaledText(
                    context = context,
                    font = CobblemonResources.DEFAULT_LARGE,
                    text = "R${assignment.radius}".text().bold(),
                    x = x + 4,
                    y = y + 2,
                    shadow = true
                )

                removeButton.setPosFloat(x + 23F, y + 13F)
                removeButton.render(context, mouseX, mouseY, partialTicks)

                // TODO: More details in tooltip when you hover over?

                context.disableScissor()
                val blockHovered =
                    mouseX.toFloat() in (blockX.toFloat()..(blockX.toFloat() + 16)) && mouseY.toFloat() in (blockY.toFloat()..(blockY.toFloat() + 16))
                if (blockHovered) {
                    val tooltip = getTooltipFromItem(MinecraftClient.getInstance(), blockItem)
                    tooltip.add(
                        "cobblemonworkers.ui.assignments.location".asTranslated(
                            assignment.blockPos.x,
                            assignment.blockPos.y,
                            assignment.blockPos.z
                        )
                    )
                    context.drawTooltip(MinecraftClient.getInstance().textRenderer, tooltip, mouseX, mouseY)
                }
                context.enableScissor(a, b, c, d)
            }

            override fun getNarration(): Text {
                // TODO: Not yet implemented
                return Text.empty()
            }

            override fun mouseClicked(d: Double, e: Double, i: Int): Boolean {
                if (removeButton.isHovered) {
                    removeButton.onPress()
                    return true
                }
                return false
            }
        }
    }
}

