package com.arcaryx.cobblemonworkers

import com.arcaryx.cobblemonworkers.client.InteractionGuiHandler
import com.arcaryx.cobblemonworkers.client.gui.ItemFilterScreenHandler
import com.arcaryx.cobblemonworkers.net.*
import com.cobblemon.mod.common.Cobblemon
import com.cobblemon.mod.common.api.net.ClientNetworkPacketHandler
import com.cobblemon.mod.common.api.net.NetworkPacket
import com.cobblemon.mod.common.api.net.ServerNetworkPacketHandler
import com.cobblemon.mod.common.platform.events.PlatformEvents
import net.minecraft.network.PacketByteBuf
import net.minecraft.screen.NamedScreenHandlerFactory
import net.minecraft.screen.ScreenHandlerType
import net.minecraft.util.Identifier
import org.apache.logging.log4j.LogManager

object CobblemonWorkers {
    const val MOD_ID: String = "cobblemonworkers"
    val LOGGER = LogManager.getLogger()

    lateinit var pluginLoader: IPluginLoader
    lateinit var furnaceAccessor: IFurnaceAccessor

    lateinit var ITEM_FILTER_SCREEN_HANDLER: ScreenHandlerType<ItemFilterScreenHandler>
    lateinit var ITEM_FILTER_SCREEN_HANDLER_FACTORY: NamedScreenHandlerFactory

    fun init() {
        PlatformEvents.SERVER_STARTED.subscribe { event ->
            PluginRegistry.build(event.server.registryManager)
        }
    }

    fun initClient() {
        InteractionGuiHandler.init()
    }

    fun registerServerBound() {
        this.createServerBound(
            RequestAssignmentCachePacket.ID,
            RequestAssignmentCachePacket::decode,
            RequestAssignmentCachePacketHandler
        )
        this.createServerBound(UpdateAssignmentPacket.ID, UpdateAssignmentPacket::decode, UpdateAssignmentPacketHandler)
        this.createServerBound(EditItemFilterPacket.ID, EditItemFilterPacket::decode, EditItemFilterPacketHandler)
    }

    fun registerClientBound() {
        this.createClientBound(AssignmentCachePacket.ID, AssignmentCachePacket::decode, AssignmentCachePacketHandler)
        this.createClientBound(
            AssignmentPickBlockPacket.ID,
            AssignmentPickBlockPacket::decode,
            AssignmentPickBlockPacketHandler
        )
        this.createClientBound(PickPluginPacket.ID, PickPluginPacket::decode, PickPluginPacketHandler)
    }


    private inline fun <reified T : NetworkPacket<T>> createServerBound(
        identifier: Identifier,
        noinline decoder: (PacketByteBuf) -> T,
        handler: ServerNetworkPacketHandler<T>
    ) {
        Cobblemon.implementation.networkManager.createServerBound(
            identifier,
            T::class,
            { message, buffer -> message.encode(buffer) },
            decoder,
            handler
        )
    }

    private inline fun <reified T : NetworkPacket<T>> createClientBound(
        identifier: Identifier,
        noinline decoder: (PacketByteBuf) -> T,
        handler: ClientNetworkPacketHandler<T>
    ) {
        Cobblemon.implementation.networkManager.createClientBound(
            identifier,
            T::class,
            { message, buffer -> message.encode(buffer) },
            decoder,
            handler
        )
    }
}