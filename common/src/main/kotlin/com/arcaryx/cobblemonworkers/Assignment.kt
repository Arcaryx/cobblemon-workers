package com.arcaryx.cobblemonworkers

import com.arcaryx.cobblemonworkers.api.ICWPluginConfig
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtHelper
import net.minecraft.network.PacketByteBuf
import net.minecraft.registry.Registries
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World
import java.util.*

data class Assignment(
    val uuid: UUID,
    val plugin: Identifier,
    val blockPos: BlockPos,
    val block: Identifier,
    val radius: Int,
    val config: ICWPluginConfig
) {

    companion object {
        private const val ASSIGNMENT_UUID: String = "cwUuid"
        private const val ASSIGNMENT_PLUGIN: String = "cwPlugin"
        private const val ASSIGNMENT_BLOCK_POS: String = "cwBlockPos"
        private const val ASSIGNMENT_BLOCK: String = "cwBlock"
        private const val ASSIGNMENT_RADIUS: String = "cwRadius"
        private const val ASSIGNMENT_CONFIG: String = "cwConfig"

        fun fromNbtCompound(nbtCompound: NbtCompound):Assignment {
            val identifier = Identifier(nbtCompound.getString(ASSIGNMENT_PLUGIN))
            val assignmentConfigNbt = nbtCompound.getCompound(ASSIGNMENT_CONFIG);

            return Assignment(
                nbtCompound.getUuid(ASSIGNMENT_UUID),
                identifier,
                NbtHelper.toBlockPos(nbtCompound.getCompound(ASSIGNMENT_BLOCK_POS)),
                Identifier(nbtCompound.getString(ASSIGNMENT_BLOCK)),
                nbtCompound.getInt(ASSIGNMENT_RADIUS),
                PluginRegistry.getPlugin(identifier)!!.getPluginConfig(assignmentConfigNbt),
            )
        }

        fun decode(buffer: PacketByteBuf): Assignment {
            val uuid = buffer.readUuid();
            val pluginIdentifier = buffer.readIdentifier()
            val blockPos = buffer.readBlockPos();
            val blockIdentifier = buffer.readIdentifier();
            val radius = buffer.readInt();
            val assignmentConfigNbt = buffer.readNbt()!!;

            return Assignment(
                uuid,
                pluginIdentifier,
                blockPos,
                blockIdentifier,
                radius,
                PluginRegistry.getPlugin(pluginIdentifier)!!.getPluginConfig(assignmentConfigNbt)
            )
        }

    }

    fun toNbt(): NbtCompound {
        val nbt = NbtCompound()
        nbt.putUuid(ASSIGNMENT_UUID, uuid)
        nbt.putString(ASSIGNMENT_PLUGIN, plugin.toString())
        nbt.put(ASSIGNMENT_BLOCK_POS, NbtHelper.fromBlockPos(blockPos))
        nbt.putString(ASSIGNMENT_BLOCK, block.toString())
        nbt.putInt(ASSIGNMENT_RADIUS, radius)
        nbt.put(ASSIGNMENT_CONFIG, config.nbtData)
        return nbt
    }

    fun encode(buffer: PacketByteBuf) {
        buffer.writeUuid(uuid)
        buffer.writeIdentifier(plugin)
        buffer.writeBlockPos(blockPos)
        buffer.writeIdentifier(block)
        buffer.writeInt(radius)
        buffer.writeNbt(config.nbtData)
    }

    fun isValid(world: World) = Registries.BLOCK.getId(world.getBlockState(blockPos).block) == block
}