package com.arcaryx.cobblemonworkers.plugin

import com.arcaryx.cobblemonworkers.CobblemonWorkers
import com.arcaryx.cobblemonworkers.api.CWPlugin
import com.arcaryx.cobblemonworkers.api.ICWPlugin
import com.arcaryx.cobblemonworkers.api.ICWPluginConfig
import com.arcaryx.cobblemonworkers.api.ICWPluginConfigFactory
import com.arcaryx.cobblemonworkers.mixin.AccessorAbstractFurnaceBlockEntity
import com.arcaryx.cobblemonworkers.nav.FacingAssignmentBlock
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import net.minecraft.block.AbstractFurnaceBlock
import net.minecraft.block.entity.AbstractFurnaceBlockEntity
import net.minecraft.nbt.NbtCompound
import net.minecraft.recipe.Recipe
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos

@CWPlugin
class FurnacePlugin : ICWPlugin<ICWPluginConfig> {
    override val configFactory = object : ICWPluginConfigFactory<ICWPluginConfig> {
        override fun create(nbtData: NbtCompound?): ICWPluginConfig = ICWPluginConfig(nbtData)
    }

    override fun getIdentifier() = Identifier(CobblemonWorkers.MOD_ID, "base_furnace")

    override fun canStart(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos, config: ICWPluginConfig) =
        isFurnaceHasRecipe(pokemonEntity, assignmentBlockPos)

    override fun getAssignmentStartNavigationConfig() = FacingAssignmentBlock(AbstractFurnaceBlock.FACING)

    private fun isFurnaceHasRecipe(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos): Boolean {
        val assignmentBlockEntity = pokemonEntity.world.getBlockEntity(assignmentBlockPos)
        if (assignmentBlockEntity !is AbstractFurnaceBlockEntity) {
            return false
        }
        val bl3 = !((assignmentBlockEntity as AccessorAbstractFurnaceBlockEntity).inventory[0]).isEmpty
        val recipe =
            if (bl3) assignmentBlockEntity.matchGetter.getFirstMatch(assignmentBlockEntity, pokemonEntity.world)
                .orElse(null) as Recipe<*> else null
        val i: Int = assignmentBlockEntity.maxCountPerStack
        return CobblemonWorkers.furnaceAccessor.canAcceptRecipeOutput(
            assignmentBlockEntity,
            pokemonEntity.world.registryManager,
            recipe,
            assignmentBlockEntity.inventory,
            i
        )
    }
}