package com.arcaryx.cobblemonworkers.plugin

import com.arcaryx.cobblemonworkers.api.ICWPluginConfig
import com.arcaryx.cobblemonworkers.gui.GuiField
import com.arcaryx.cobblemonworkers.gui.GuiFieldType
import net.minecraft.item.Item
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtElement
import net.minecraft.nbt.NbtList

class FarmlandConfig(nbtData: NbtCompound? = null) : ICWPluginConfig(nbtData) {

    companion object {
        const val CONFIG_SHOULD_PLANT: String = "should_plant"
        const val CONFIG_SHOULD_GROW: String = "should_grow"
        const val CONFIG_SHOULD_HARVEST: String = "should_harvest"
    }

    override fun getGuiFields(): List<GuiField> = listOf(
        GuiField(GuiFieldType.TYPE_ITEM_FILTER, CONFIG_SHOULD_PLANT),
        GuiField(GuiFieldType.TYPE_CHECKBOX, CONFIG_SHOULD_GROW),
        GuiField(GuiFieldType.TYPE_CHECKBOX, CONFIG_SHOULD_HARVEST)
    )

    override fun getDefaultNbtData() = NbtCompound().apply {
        put(CONFIG_SHOULD_PLANT, NbtList())
        putBoolean(CONFIG_SHOULD_GROW, true)
        putBoolean(CONFIG_SHOULD_HARVEST, true)
    }

    fun shouldPlant() = !nbtData.getList(CONFIG_SHOULD_PLANT, NbtElement.STRING_TYPE.toInt()).isEmpty()
    fun shouldGrow() = nbtData.getBoolean(CONFIG_SHOULD_GROW)
    fun shouldHarvest() = nbtData.getBoolean(CONFIG_SHOULD_HARVEST)

}