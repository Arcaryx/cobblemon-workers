package com.arcaryx.cobblemonworkers.plugin

import com.arcaryx.cobblemonworkers.CobblemonWorkers
import com.arcaryx.cobblemonworkers.api.CWPlugin
import com.arcaryx.cobblemonworkers.api.ICWPlugin
import com.arcaryx.cobblemonworkers.api.ICWPluginConfigFactory
import com.arcaryx.cobblemonworkers.gui.GuiField
import com.arcaryx.cobblemonworkers.gui.GuiFieldType
import com.arcaryx.cobblemonworkers.nav.MoveOnTopOfAssignmentBlock
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import net.minecraft.block.CropBlock
import net.minecraft.item.Item
import net.minecraft.nbt.NbtCompound
import net.minecraft.server.world.ServerWorld
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World

@CWPlugin
class FarmlandPlugin : ICWPlugin<FarmlandConfig> {
    override val configFactory = object : ICWPluginConfigFactory<FarmlandConfig> {
        override fun create(nbtData: NbtCompound?): FarmlandConfig = FarmlandConfig(nbtData)
    }

    override fun getIdentifier() = Identifier(CobblemonWorkers.MOD_ID, "base_farmland")

    override fun canStart(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos, config: FarmlandConfig): Boolean {
        return (shouldPlant(pokemonEntity, assignmentBlockPos, config)) ||
                shouldGrow(pokemonEntity, assignmentBlockPos, config) ||
                shouldHarvest(pokemonEntity, assignmentBlockPos, config)
    }

    override fun getAssignmentStartNavigationConfig() = MoveOnTopOfAssignmentBlock()

    override fun tick(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos, config: FarmlandConfig) {
        val cropBlockPos = assignmentBlockPos.up()
        val cropBlockState = pokemonEntity.world.getBlockState(cropBlockPos)
        val cropBlock = cropBlockState.block
        if (cropBlock is CropBlock && pokemonEntity.world is ServerWorld) {
            if (shouldPlant(pokemonEntity, assignmentBlockPos, config)) {
                // TODO: Planting logic
                // If inv contains item, use that, otherwise search for valid floating item

            } else if (shouldGrow(pokemonEntity, assignmentBlockPos, config)) {
                cropBlock.randomTick(
                    cropBlockState,
                    pokemonEntity.world as ServerWorld,
                    cropBlockPos,
                    pokemonEntity.world.random
                )
            } else if (shouldHarvest(pokemonEntity, assignmentBlockPos, config)) {
                pokemonEntity.world.breakBlock(cropBlockPos, true, pokemonEntity)
            }
        }
    }

    override fun clickOffset(world: World, clickPos: BlockPos): BlockPos {
        val clickBlockState = world.getBlockState(clickPos)
        if (clickBlockState.block is CropBlock) {
            return clickPos.down()
        }
        return clickPos
    }

    private fun shouldGrow(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos, config: FarmlandConfig) : Boolean {
        return config.shouldGrow() && hasGrowableCrop(pokemonEntity, assignmentBlockPos)
    }

    private fun hasGrowableCrop(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos): Boolean {
        val cropBlockPos = assignmentBlockPos.up()
        val cropBlockState = pokemonEntity.world.getBlockState(cropBlockPos)
        val cropBlock = cropBlockState.block
        if (cropBlock !is CropBlock) {
            return false
        }
        return !cropBlock.isMature(cropBlockState) && cropBlock.canGrow(
            pokemonEntity.world,
            pokemonEntity.world.random,
            cropBlockPos,
            cropBlockState
        )
    }


    private fun shouldHarvest(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos, config: FarmlandConfig) : Boolean {
        return config.shouldHarvest() && hasMatureCrop(pokemonEntity, assignmentBlockPos)
    }

    private fun hasMatureCrop(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos): Boolean {
        val cropBlockPos = assignmentBlockPos.up()
        val cropBlockState = pokemonEntity.world.getBlockState(cropBlockPos)
        val cropBlock = cropBlockState.block
        if (cropBlock !is CropBlock) {
            return false
        }
        return cropBlock.isMature(cropBlockState)
    }

    private fun shouldPlant(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos, config: FarmlandConfig) : Boolean {
        return false // TODO: Planting logic
    }
}