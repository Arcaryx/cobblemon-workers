package com.arcaryx.cobblemonworkers.gui

data class GuiField(
    val type: GuiFieldType,
    val name: String
)