package com.arcaryx.cobblemonworkers.gui

enum class GuiFieldType {
    TYPE_CHECKBOX,
    TYPE_INTEGER,
    TYPE_ITEM_FILTER,
    TYPE_BLOCK_FILTER,
}