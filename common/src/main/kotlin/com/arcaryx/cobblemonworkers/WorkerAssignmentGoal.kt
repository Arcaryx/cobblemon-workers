package com.arcaryx.cobblemonworkers

import com.arcaryx.cobblemonworkers.api.ICWPlugin
import com.arcaryx.cobblemonworkers.api.ICWPluginConfig
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import com.cobblemon.mod.common.entity.pokemon.ai.PokemonNavigation
import com.cobblemon.mod.common.util.toVec3d
import net.minecraft.entity.ai.goal.Goal
import net.minecraft.entity.ai.pathing.PathNode
import net.minecraft.util.math.BlockPos
import java.util.*
import kotlin.math.sqrt

class WorkerAssignmentGoal(private val pokemonEntity: PokemonEntity) : Goal() {
    private lateinit var cwPlugin: ICWPlugin<ICWPluginConfig>
    private lateinit var assignment: Assignment
    private lateinit var goalBlockPos: BlockPos
    private lateinit var navPos: BlockPos
    private var freezeTicks: Int = 0

    init {
        this.controls = EnumSet.of(Control.MOVE, Control.LOOK)
    }

    override fun canStart(): Boolean {
        val cwPokemon = CWPokemon.fromPokemonEntity(pokemonEntity) ?: return false
        cwPokemon.reconcileAssignments()

        for (pokemonAssignment in cwPokemon.assignments) {
            val plugin = PluginRegistry.getPlugin(pokemonAssignment.plugin) ?: continue
            val random = pokemonEntity.world.random
            for (i in 1..10) { // TODO: Config this
                val blockPos = BlockPos(
                    random.nextBetween(
                        pokemonAssignment.blockPos.x - pokemonAssignment.radius,
                        pokemonAssignment.blockPos.x + pokemonAssignment.radius
                    ),
                    random.nextBetween(
                        pokemonAssignment.blockPos.y - pokemonAssignment.radius,
                        pokemonAssignment.blockPos.y + pokemonAssignment.radius
                    ),
                    random.nextBetween(
                        pokemonAssignment.blockPos.z - pokemonAssignment.radius,
                        pokemonAssignment.blockPos.z + pokemonAssignment.radius
                    )
                )
                if (plugin.canStart(pokemonEntity, blockPos, pokemonAssignment.config)) {
                    cwPlugin = plugin
                    assignment = pokemonAssignment
                    goalBlockPos = blockPos

                    val assignmentStartNavigationConfig = cwPlugin.getAssignmentStartNavigationConfig()
                    navPos = assignmentStartNavigationConfig.getNavPos(pokemonEntity, blockPos)

                    return true
                }
            }
        }
        return false
    }

    override fun shouldContinue(): Boolean {
        val cwPokemon = CWPokemon.fromPokemonEntity(pokemonEntity) ?: return false
        if (cwPokemon.assignments.none { it.uuid == assignment.uuid }) {
            return false
        }
        return cwPlugin.shouldContinue(pokemonEntity, goalBlockPos, assignment.config)
    }

    override fun start() {
        moveToNavPos()
    }

    override fun stop() {
        pokemonEntity.navigation.stop()
        ServerStateManager.removeWorker(goalBlockPos, pokemonEntity.world, pokemonEntity, assignment.uuid)
    }

    override fun tick() {
        val targetPos = goalBlockPos.toVec3d().add(0.5, 0.0, 0.5)
        val pokemonPos = pokemonEntity.pos
        if (pokemonPos.distanceTo(targetPos) < cwPlugin.getActivationDist() + sqrt(2 * pokemonEntity.width)) {
            // TODO: Fix cobblemon running bug (probably need to make an issue for this)
            //if (!pokemonEntity.navigation.isIdle) {
            //    freezeTicks++
            //    if (freezeTicks > 20) {
            //        pokemonEntity.navigation.stop()
            //    }
            //}
            cwPlugin.tick(pokemonEntity, goalBlockPos, assignment.config)
            pokemonEntity.navigation.setSpeed(0.75) // Slow down so don't overshoot target pos
            ServerStateManager.addWorker(goalBlockPos, pokemonEntity.world, pokemonEntity, assignment.uuid)
            pokemonEntity.lookControl.lookAt(
                goalBlockPos.x + 0.5,
                goalBlockPos.y + 0.5,
                goalBlockPos.z + 0.5
            )
        } else {
            if (pokemonEntity.navigation.isIdle) {
                moveToNavPos()
            }
            ServerStateManager.removeWorker(goalBlockPos, pokemonEntity.world, pokemonEntity, assignment.uuid)
        }

    }

    private fun moveToNavPos() {
        pokemonEntity.navigation.startMovingTo(
            x = navPos.x.toDouble(),
            y = navPos.y.toDouble(),
            z = navPos.z.toDouble(),
            speed = 1.25,
            navigationContext = PokemonNavigation.NavigationContext(
                sprinting = true,
                destinationProximity = 0.0F
            )
        )
        pokemonEntity.navigation.currentPath?.length = pokemonEntity.navigation.currentPath!!.length + 1
        pokemonEntity.navigation.currentPath?.setNode(
            pokemonEntity.navigation.currentPath!!.length - 1,
            PathNode(navPos.x, navPos.y, navPos.z)
        )
        freezeTicks = 0
    }
}