package com.arcaryx.cobblemonworkers

interface IPluginLoader {
    fun load()
}