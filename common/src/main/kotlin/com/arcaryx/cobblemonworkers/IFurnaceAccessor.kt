package com.arcaryx.cobblemonworkers

import net.minecraft.block.entity.AbstractFurnaceBlockEntity
import net.minecraft.item.ItemStack
import net.minecraft.recipe.Recipe
import net.minecraft.registry.DynamicRegistryManager
import net.minecraft.util.collection.DefaultedList

interface IFurnaceAccessor {
    fun canAcceptRecipeOutput(
        blockEntity: AbstractFurnaceBlockEntity,
        registryManager: DynamicRegistryManager?,
        recipe: Recipe<*>?,
        slots: DefaultedList<ItemStack?>?,
        count: Int
    ): Boolean
}