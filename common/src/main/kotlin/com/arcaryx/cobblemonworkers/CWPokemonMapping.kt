package com.arcaryx.cobblemonworkers

import com.mojang.serialization.Codec
import com.mojang.serialization.codecs.RecordCodecBuilder
import net.minecraft.registry.Registry
import net.minecraft.registry.RegistryKey
import net.minecraft.util.Identifier

class CWPokemonMapping(val species: String, val form: String, val workSuitabilityMapping: Map<Identifier, Int>) {

    init {
        CobblemonWorkers.LOGGER.info("Loading CWPokemonMapping for: $species ($form)")
    }

    companion object {
        var KEY: RegistryKey<Registry<CWPokemonMapping>>? = null
        var NAME: String = "pokemon_mapping"

        val CODEC: Codec<CWPokemonMapping> =
            RecordCodecBuilder.create { instance: RecordCodecBuilder.Instance<CWPokemonMapping> ->
                instance.group(
                    Codec.STRING.fieldOf("pokemon_species")
                        .forGetter { obj: CWPokemonMapping -> obj.species },
                    Codec.STRING.fieldOf("pokemon_form")
                        .forGetter { obj: CWPokemonMapping -> obj.form },
                    Codec.unboundedMap(
                        Identifier.CODEC,
                        Codec.INT
                    ).fieldOf("work_suitability").forGetter { obj: CWPokemonMapping -> obj.workSuitabilityMapping }
                ).apply(
                    instance
                ) { species: String, form: String, workSuitabilityMapping: Map<Identifier, Int> ->
                    CWPokemonMapping(
                        species,
                        form,
                        workSuitabilityMapping
                    )
                }
            }
    }
}