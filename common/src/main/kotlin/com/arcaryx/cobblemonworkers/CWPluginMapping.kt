package com.arcaryx.cobblemonworkers

import com.arcaryx.cobblemonworkers.CobblemonWorkers.LOGGER
import com.mojang.serialization.Codec
import com.mojang.serialization.codecs.RecordCodecBuilder
import net.minecraft.registry.Registry
import net.minecraft.registry.RegistryKey
import net.minecraft.util.Identifier

class CWPluginMapping(val pluginId: Identifier, val blockMapping: Map<Identifier, Identifier>) {
    init {
        LOGGER.info("Loading CWPluginMapping for: $pluginId")
    }

    companion object {
        var KEY: RegistryKey<Registry<CWPluginMapping>>? = null
        var NAME: String = "plugin_mapping"

        val CODEC: Codec<CWPluginMapping> =
            RecordCodecBuilder.create { instance: RecordCodecBuilder.Instance<CWPluginMapping> ->
                instance.group(
                    Identifier.CODEC.fieldOf("plugin_id")
                        .forGetter { obj: CWPluginMapping -> obj.pluginId },
                    Codec.unboundedMap(
                        Identifier.CODEC,
                        Identifier.CODEC
                    ).fieldOf("block_mapping").forGetter { obj: CWPluginMapping -> obj.blockMapping }
                ).apply(
                    instance
                ) { pluginId: Identifier, blockMapping: Map<Identifier, Identifier> ->
                    CWPluginMapping(
                        pluginId,
                        blockMapping
                    )
                }
            }
    }
}