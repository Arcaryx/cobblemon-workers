package com.arcaryx.cobblemonworkers.api

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class CWPlugin(val value: String = "")