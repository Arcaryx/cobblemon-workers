package com.arcaryx.cobblemonworkers.api

import com.arcaryx.cobblemonworkers.gui.GuiField
import com.arcaryx.cobblemonworkers.nav.AbstractAssignmentStartNavigationConfig
import com.arcaryx.cobblemonworkers.nav.MoveDirectlyToAssignmentBlock
import com.cobblemon.mod.common.entity.pokemon.PokemonEntity
import net.minecraft.nbt.NbtCompound
import net.minecraft.util.Identifier
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World

interface ICWPluginConfigFactory<ConfigType : ICWPluginConfig> {
    fun create(nbtData: NbtCompound? = null): ConfigType
}

interface ICWPlugin<ConfigType : ICWPluginConfig> {
    val configFactory: ICWPluginConfigFactory<ConfigType>

    fun getIdentifier(): Identifier

    fun getActivationDist() = 1.0

    fun canStart(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos, config: ConfigType): Boolean

    fun getAssignmentStartNavigationConfig(): AbstractAssignmentStartNavigationConfig = MoveDirectlyToAssignmentBlock()

    fun shouldContinue(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos, config: ConfigType) =
        canStart(pokemonEntity, assignmentBlockPos, config)

    fun tick(pokemonEntity: PokemonEntity, assignmentBlockPos: BlockPos, config: ConfigType) {}

    fun clickOffset(world: World, clickPos: BlockPos) = clickPos

    fun getPluginConfig(nbtData: NbtCompound): ConfigType {
        return configFactory.create(nbtData)
    }
}