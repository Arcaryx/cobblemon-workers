package com.arcaryx.cobblemonworkers.api

import com.arcaryx.cobblemonworkers.gui.GuiField
import net.minecraft.nbt.NbtCompound

open class ICWPluginConfig(nbtData: NbtCompound? = null) {
    val nbtData: NbtCompound by lazy { nbtData ?: getDefaultNbtData() }

    open fun getGuiFields() = emptyList<GuiField>()

    open fun getDefaultNbtData() = NbtCompound()
}