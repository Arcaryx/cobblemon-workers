import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import utilities.ACCESS_WIDENER

plugins {
    id("java")
    id("java-library")
    id("net.kyori.indra")
    id("net.kyori.indra.git")

    id("dev.architectury.loom")
    id("architectury-plugin")
    kotlin("jvm")
}

group = rootProject.group
version = rootProject.version
description = rootProject.description

indra {
    javaVersions {
        minimumToolchain(17)
        target(17)
    }
}

repositories {
    mavenCentral()
    // Cobblemon
    maven("https://maven.impactdev.net/repository/development/")
}

architectury {
    minecraft = project.property("minecraft_version").toString()
}

loom {
    silentMojangMappingsLicense()
    accessWidenerPath.set(project(":common").file(ACCESS_WIDENER))

    mixin {
        defaultRefmapName.set("cobblemonworkers-${project.name}-refmap.json")
    }
}

dependencies {
    minecraft("net.minecraft:minecraft:${rootProject.property("minecraft_version")}")
    mappings("net.fabricmc:yarn:${rootProject.property("yarn_version")}:v2")
}

tasks {
    withType<JavaCompile> {
        options.compilerArgs.add("-Xlint:-processing,-classfile,-serial")
    }

    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "17"
    }
}