plugins {
    id("cw.root-conventions")
}

group = project.property("mod_group") as String
version = project.property("mod_version") as String