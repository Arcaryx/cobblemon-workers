plugins {
    id("cw.platform-conventions")
}

architectury {
    platformSetupLoomIde()
    forge()
}

loom {
    forge {
        convertAccessWideners.set(true)

        mixinConfig("cobblemonworkers.mixins.json")
        mixinConfig("cobblemonworkers-common.mixins.json")
    }
}


repositories {
    maven(url = "${rootProject.projectDir}/deps")
    maven(url = "https://thedarkcolour.github.io/KotlinForForge/")
    maven(url = "https://api.modrinth.com/maven")
    mavenLocal()
}


dependencies {
    forge(libs.forge)
    modImplementation(libs.cobblemon.forge)

    implementation(project(":common", configuration = "namedElements")) {
        isTransitive = false
    }
    implementation(libs.kotlin.forge)
    "developmentForge"(project(":common", configuration = "namedElements")) {
        isTransitive = false
    }
    bundle(project(path = ":common", configuration = "transformProductionForge")) {
        isTransitive = false
    }
    testImplementation(project(":common", configuration = "namedElements"))
}

tasks {
    shadowJar {
        exclude("architectury-common.accessWidener")
        exclude("architectury.common.json")
    }

    processResources {
        val propertiesMap = project.properties.entries.associate { it.key.toString() to it.value.toString() }
        inputs.properties(propertiesMap)
        filesMatching("META-INF/mods.toml") {
            expand(propertiesMap)
        }
    }
}

tasks {
    sourcesJar {
        val depSources = project(":common").tasks.sourcesJar
        duplicatesStrategy = DuplicatesStrategy.EXCLUDE
        dependsOn(depSources)
        from(depSources.get().archiveFile.map { zipTree(it) }) {
            exclude("architectury.accessWidener")
        }
    }
}
