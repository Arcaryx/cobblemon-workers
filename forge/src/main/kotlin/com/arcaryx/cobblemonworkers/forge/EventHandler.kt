package com.arcaryx.cobblemonworkers.forge

import com.arcaryx.cobblemonworkers.CWPluginMapping
import com.arcaryx.cobblemonworkers.CWPokemonMapping
import net.minecraft.registry.RegistryKey
import net.minecraft.util.Identifier
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.registries.DataPackRegistryEvent.NewRegistry

object EventHandler {
    @SubscribeEvent
    @JvmStatic
    fun registerDatapack(event: NewRegistry) {
        CWPluginMapping.KEY = RegistryKey.ofRegistry(Identifier(CWPluginMapping.NAME))
        event.dataPackRegistry(CWPluginMapping.KEY, CWPluginMapping.CODEC)

        CWPokemonMapping.KEY = RegistryKey.ofRegistry(Identifier(CWPokemonMapping.NAME))
        event.dataPackRegistry(CWPokemonMapping.KEY, CWPokemonMapping.CODEC)
    }
}