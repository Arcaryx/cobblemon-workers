package com.arcaryx.cobblemonworkers.forge

import com.arcaryx.cobblemonworkers.CobblemonWorkers
import com.arcaryx.cobblemonworkers.IPluginLoader
import com.arcaryx.cobblemonworkers.PluginRegistry
import com.arcaryx.cobblemonworkers.api.CWPlugin
import com.arcaryx.cobblemonworkers.api.ICWPlugin
import com.arcaryx.cobblemonworkers.api.ICWPluginConfig
import net.minecraftforge.fml.ModList
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext

class PluginLoaderForge : IPluginLoader {

    init {
        FMLJavaModLoadingContext.get().modEventBus.addListener(this::onLoadComplete)
    }

    // https://github.com/Snownee/Jade/blob/e69e422e748c1f5258efd76cf6285c372afa1157/src/main/java/snownee/jade/util/CommonProxy.java#L326
    private fun onLoadComplete(event: FMLLoadCompleteEvent) {
        load()
    }

    override fun load() {
        val classNames: List<String> = ModList.get().getAllScanData()
            .flatMap { it.annotations.asSequence() }
            .filter { annotation ->
                val annotationTypeName = annotation.annotationType().className
                if (annotationTypeName == CWPlugin::class.java.name) {
                    val required = annotation.annotationData().getOrDefault("value", "") as String
                    required.isEmpty() || ModList.get().isLoaded(required)
                } else {
                    false
                }
            }
            .map { it.memberName }
            .toList()

        for (className in classNames) {
            CobblemonWorkers.LOGGER.info("Loading plugin at $className")
            try {
                val clazz = Class.forName(className)
                if (ICWPlugin::class.java.isAssignableFrom(clazz)) {
                    val plugin = clazz.getDeclaredConstructor().newInstance() as ICWPlugin<*>
                    PluginRegistry.register(plugin as ICWPlugin<ICWPluginConfig>)
                }
            } catch (ex: Throwable) {
                CobblemonWorkers.LOGGER.error("Error loading plugin at $className", ex)
            }
        }
    }
}