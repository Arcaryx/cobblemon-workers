package com.arcaryx.cobblemonworkers.forge

import com.arcaryx.cobblemonworkers.CobblemonWorkers
import com.arcaryx.cobblemonworkers.client.gui.ItemFilterScreen
import net.minecraft.client.gui.screen.ingame.HandledScreens
import net.minecraftforge.eventbus.api.SubscribeEvent
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent


object ClientEventHandler {
    @SubscribeEvent
    @JvmStatic
    fun onClientSetup(event: FMLClientSetupEvent) {
        CobblemonWorkers.ITEM_FILTER_SCREEN_HANDLER = CobblemonWorkersForge.ITEM_FILTER_MENU.get()
        HandledScreens.register(CobblemonWorkers.ITEM_FILTER_SCREEN_HANDLER, ::ItemFilterScreen)
    }
}