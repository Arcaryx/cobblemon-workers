package com.arcaryx.cobblemonworkers.forge

import com.arcaryx.cobblemonworkers.CobblemonWorkers
import com.arcaryx.cobblemonworkers.client.gui.ItemFilterScreen
import com.arcaryx.cobblemonworkers.client.gui.ItemFilterScreenHandler
import net.minecraft.resource.featuretoggle.FeatureFlags
import net.minecraft.screen.ScreenHandlerType
import net.minecraftforge.api.distmarker.Dist
import net.minecraftforge.fml.DistExecutor
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext
import net.minecraftforge.registries.DeferredRegister
import net.minecraftforge.registries.ForgeRegistries
import net.minecraftforge.registries.RegistryObject

@Mod(CobblemonWorkers.MOD_ID)
class CobblemonWorkersForge {

    companion object {
        var MENU_TYPES: DeferredRegister<ScreenHandlerType<*>> = DeferredRegister.create(ForgeRegistries.MENU_TYPES, CobblemonWorkers.MOD_ID)
        var ITEM_FILTER_MENU: RegistryObject<ScreenHandlerType<ItemFilterScreenHandler>> = MENU_TYPES.register(ItemFilterScreen.NAME) {
            ScreenHandlerType({ syncId, playerInventory ->
                ItemFilterScreenHandler(syncId, playerInventory)
            }, FeatureFlags.VANILLA_FEATURES)
        }
    }

    init {
        CobblemonWorkers.init()
        CobblemonWorkers.furnaceAccessor = FurnaceAccessorForge()
        DistExecutor.safeRunWhenOn(Dist.CLIENT) {
            DistExecutor.SafeRunnable(CobblemonWorkers::initClient)
        }
        CobblemonWorkers.pluginLoader = PluginLoaderForge()
        val modBus = FMLJavaModLoadingContext.get().modEventBus
        modBus.register(EventHandler::class.java)
        modBus.register(ClientEventHandler::class.java)
        MENU_TYPES.register(modBus)
        CobblemonWorkers.ITEM_FILTER_SCREEN_HANDLER_FACTORY = ItemFilterScreenFactoryForge()
    }
}