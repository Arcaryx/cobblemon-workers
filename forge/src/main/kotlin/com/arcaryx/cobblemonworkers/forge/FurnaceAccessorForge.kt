package com.arcaryx.cobblemonworkers.forge

import com.arcaryx.cobblemonworkers.IFurnaceAccessor
import net.minecraft.block.entity.AbstractFurnaceBlockEntity
import net.minecraft.item.ItemStack
import net.minecraft.recipe.Recipe
import net.minecraft.registry.DynamicRegistryManager
import net.minecraft.util.collection.DefaultedList

class FurnaceAccessorForge : IFurnaceAccessor {
    private val canAcceptRecipeOutputMethod =
        AbstractFurnaceBlockEntity::class.java.declaredMethods.find { it.name == "canAcceptRecipeOutput" }
            ?: throw NoSuchMethodException("canAcceptRecipeOutput method not found")

    init {
        canAcceptRecipeOutputMethod.isAccessible = true
    }

    override fun canAcceptRecipeOutput(
        blockEntity: AbstractFurnaceBlockEntity,
        registryManager: DynamicRegistryManager?,
        recipe: Recipe<*>?,
        slots: DefaultedList<ItemStack?>?,
        count: Int
    ): Boolean {
        return canAcceptRecipeOutputMethod.invoke(blockEntity, registryManager, recipe, slots, count) as Boolean
    }
}