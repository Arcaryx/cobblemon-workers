package com.arcaryx.cobblemonworkers.forge

import com.arcaryx.cobblemonworkers.client.gui.ItemFilterScreen
import com.arcaryx.cobblemonworkers.client.gui.ItemFilterScreenHandler
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.screen.NamedScreenHandlerFactory
import net.minecraft.screen.ScreenHandler
import net.minecraft.text.Text

class ItemFilterScreenFactoryForge() : NamedScreenHandlerFactory {
    override fun createMenu(syncId: Int, playerInventory: PlayerInventory, player: PlayerEntity): ScreenHandler {
        return ItemFilterScreenHandler(syncId, playerInventory)
    }

    override fun getDisplayName(): Text {
        return ItemFilterScreen.TITLE
    }
}